package com.restobrownegg.placepicker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLngBounds;
import com.restobrownegg.R;
import com.restobrownegg.tasks.ZomatoTask;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import static android.app.Activity.RESULT_OK;

/**
 * This class is a fragment that will be getting the nearby restaurants and displaying them into
 * a list view with the help of the ZomatoTask.
 *
 * Great tutorial on this can be found at:
 * - https://developers.google.com/places/android-api/placepicker
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class PlacePickerFragment extends Fragment {


    private final String CLASS_NAME="PlacePickerFragment";
    private final int PLACE_PICKER_REQUEST = 1;
    private View v;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_near_resto, container, false);
        Log.e(CLASS_NAME,"onCreateView");

        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

            // Fix the instance error found at:
            // - http://stackoverflow.com/questions/36218434/non-static-method-isgoogleplayservicesavailable-and-geterrordialog-cannot-be-ref
            GoogleApiAvailability googleApi = GoogleApiAvailability.getInstance();
            int resultCode = googleApi.isGooglePlayServicesAvailable(getContext());
            if(resultCode != ConnectionResult.SUCCESS)
                googleApi.getErrorDialog(getActivity(),resultCode,1).show();
            else
                startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        }
        catch(GooglePlayServicesNotAvailableException exception)
        {
            Log.e(CLASS_NAME,"threw a google play services not available exception");
        }
        catch (GooglePlayServicesRepairableException exception)
        {
            Log.e(CLASS_NAME,"threw a google play services repairable exception");


        }

        return v;
    }

    /**
     * This method will be running the essential ZomatoTask in order to later populate the list view.
     * @param requestCode int
     * @param resultCode int
     * @param data Intent
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                LatLngBounds place = PlacePicker.getLatLngBounds(data);
                try {

                    // Get the longitude and latitude.
                    double latitude = place.northeast.latitude;
                    double longitude = place.northeast.longitude;
                    Log.i(CLASS_NAME,"Latitude: "+latitude);
                    Log.i(CLASS_NAME,"Longitude: "+longitude);

                    // Defune the url request that will be sent to zomato.
                    URL urlString = new URL("https://developers.zomato.com/api/v2.1/geocode?lat=" + URLEncoder.encode(latitude + "", "UTF-8") + "&lon=" + URLEncoder.encode(longitude + "", "UTF-8"));

                    // The list view to populate.
                    ListView listView = (ListView) v.findViewById(R.id.restoList);

                    // Execute zomato task.
                    ZomatoTask task = new ZomatoTask(getActivity(), listView,latitude,longitude);
                    task.execute(urlString);

                    Toast.makeText(getActivity(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                }
                catch(UnsupportedEncodingException exception)
                {
                    Log.e(CLASS_NAME, "threw an unsupported encoding exception");
                }
                catch(MalformedURLException mUrlE)
                {
                    Log.e(CLASS_NAME, "threw a malformed URL exception");
                }
            }
        }
    }


}

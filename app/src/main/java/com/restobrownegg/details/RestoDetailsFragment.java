package com.restobrownegg.details;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.restobrownegg.R;
import com.restobrownegg.dao.DBHelper;

/**
 * This class will be displaying all the data related to a specific restaurant. It knows which
 * resto to display by getting the resto's id from the intent's bundle.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class RestoDetailsFragment extends Fragment {

    private final String CLASS_NAME = "RestoDetailsFragment";

    private static DBHelper dbh;

    // Fields in the view to display resto's info.
    private TextView restoNameLbl;
    private TextView genreLbl;
    private TextView pricingLbl;
    private TextView phoneNumberLbl;
    private TextView addressLbl;
    private TextView cityLbl;
    private TextView postalLbl;
    private TextView latitudeLbl;
    private TextView longitudeLbl;
    private TextView createdLbl;
    private TextView updatedLbl;


    /**
     * Return a new instance of RestoDetailsFragment with a given index depending on which
     * restaurant to show.
     *
     * @param index representation of a restaurant
     * @return A new instance of RestoDetailsFragment
     */
    public static RestoDetailsFragment newInstance(int index) {
        RestoDetailsFragment fragment = new RestoDetailsFragment();

        // Supply index as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * Returns the index of the current restaurant displayed on screen.
     *
     * @return index representation of a restaurant
     */
    public int getShownIndex() {
        return getArguments().getInt("index", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(CLASS_NAME, "onCreateView()");
        View v = inflater.inflate(R.layout.resto_details_info, container, false);

        String restoName = "";
        String genre = "";
        String pricing = "";
        String phoneNumber = "";
        String address = "";
        String city = "";
        String postal = "";
        String latitude = "";
        String longitude = "";
        String created = "";
        String updated = "";

        // Get the resto's id that you want to display.
        int restoId = getArguments().getInt("restoId", 0);
        Log.i(CLASS_NAME, "onCreateView() restoId: " + restoId);

        dbh = new DBHelper(getActivity());

        // Get the restaurant by its id.
        Cursor restoCursor = dbh.getRestoInfo(restoId);
        Log.i(CLASS_NAME, "onCreateView() restos found: " + restoCursor.getCount());

        // Check if any restaurants was found. If so, get the informations related to that resto.
        if(restoCursor.moveToNext()){
            restoName = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_RESTO_NAME));
            genre = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_GENRE));
            pricing = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_PRICE_RANGE));
            phoneNumber = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_PHONE_NUMBER));
            address = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_ADDRESS));
            city = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_CITY));
            postal = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_POSTAL_CODE));
            latitude = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_LATITUDE));
            longitude = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_LONGITUDE));
            created = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_CREATED_DATE));
            updated = restoCursor.getString(restoCursor.getColumnIndex(dbh.COL_UPDATED_DATE));
        }
        // Close the cursor containing info about the resto.
        restoCursor.close();

        // Get a handle on each text view in order to display the restaurant's info.
        restoNameLbl = (TextView) v.findViewById(R.id.restoNameLbl);
        genreLbl = (TextView) v.findViewById(R.id.genreLbl);
        pricingLbl = (TextView) v.findViewById(R.id.pricingLbl);
        phoneNumberLbl = (TextView) v.findViewById(R.id.phoneNumberLbl);
        addressLbl = (TextView) v.findViewById(R.id.addressLbl);
        cityLbl = (TextView) v.findViewById(R.id.cityLbl);
        postalLbl = (TextView) v.findViewById(R.id.postalLbl);
        latitudeLbl = (TextView) v.findViewById(R.id.latitudeLbl);
        longitudeLbl = (TextView) v.findViewById(R.id.longitudeLbl);
        createdLbl = (TextView) v.findViewById(R.id.createdLbl);
        updatedLbl = (TextView) v.findViewById(R.id.updatedLbl);

        restoNameLbl.setText(restoName);
        genreLbl.setText(genre);
        pricingLbl.setText(pricing);
        phoneNumberLbl.setText(phoneNumber);
        addressLbl.setText(address);
        cityLbl.setText(city);
        postalLbl.setText(postal);
        latitudeLbl.setText(latitude);
        longitudeLbl.setText(longitude);
        createdLbl.setText(created);
        updatedLbl.setText(updated);

        // Set a click listener on the resto's name.
        restoNameLbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView restoName = (TextView) view.findViewById(R.id.restoNameLbl);

                // Open a google search for the resto's name.
                Uri uri = Uri.parse("http://www.google.com/#q=" +restoName.getText().toString());
                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                startActivity(intent);
            }
        });

        return v;
    }
}

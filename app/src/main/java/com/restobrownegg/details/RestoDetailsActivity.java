package com.restobrownegg.details;

import android.content.res.Configuration;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

/**
 * This class will be opening the RestoDetailsFragment as if it was an activity. It checks if
 * the screen is in portrait mode or not. This class will kill itself if it's in landscape mode. Also,
 * it will pass all the extra data to the fragment in order to display the data of a specific resto.
 * The extra data contains the item clicked.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class RestoDetailsActivity extends AppCompatActivity {

    private final String CLASS_NAME = "RestoDetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(CLASS_NAME, "onCreate()");

        // End this activity if the screen is in landscape mode.
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            finish();
            return;
        }

        if(savedInstanceState == null){

            RestoDetailsFragment details = new RestoDetailsFragment();

            // Get the index for the item selected in list.
            details.setArguments(getIntent().getExtras());

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(android.R.id.content, details);
            ft.commit();
        }
    }
}

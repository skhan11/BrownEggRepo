package com.restobrownegg.details;

import android.content.res.Configuration;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

/**
 * This class will launch the ShowFragment as if it was an activity. This is used for when the screen
 * is in portrait mode, which is why it will destroy itself if it detects that the screen is in
 * landscape mode. It also passes all extra data to the fragment since this class is doing nothing
 * but launching the fragment.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class ShowActivity extends AppCompatActivity {

    private final String CLASS_NAME = "ShowActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(CLASS_NAME, "onCreate()");

        // End this activity if the screen is in landscape mode.
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            finish();
            return;
        }

        if(savedInstanceState == null){
            ShowFragment details = new ShowFragment();

            // Get the index for the item selected in list.
            details.setArguments(getIntent().getExtras());

            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(android.R.id.content, details);
            ft.commit();
        }
    }
}

package com.restobrownegg.details;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.restobrownegg.R;
import com.restobrownegg.near.ReviewActivity;
import com.restobrownegg.dao.DBHelper;

/**
 * This class is similar to the RestoDetails class but it accepts the restaurant's information
 * through a bundle. This class is usefull for when we seek nearby restaurants, not from the database.
 * It will also enable the user to modify the fields of the restaurant if it was previously added to
 * the database. It allows to add, modify, delete the nearby restos from current database.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */

public class ShowFragment extends Fragment {

    private final String CLASS_NAME = "ShowFragment";

    // Handle on all the fields of the restaurant.
    private EditText editRestoName;
    private EditText editRestoGenre;
    private EditText editRestoPriceRange;
    private EditText editRestoAddress;
    private EditText editRestoCity;
    private EditText editRestoPostalCode;
    private EditText editRestoLatitude;
    private EditText editRestoLongitude;
    private EditText editCreatedDate;
    private EditText editUpdatedDate;
    private EditText editTelephoneNumber;

    // The values of the restaurant fields.
    private String restoName;
    private String restoGenre;
    private int restoPriceRange;
    private String restoAddress;
    private String restoCity;
    private String restoPostalCode;
    private double restoLatitude;
    private double restoLongitude;
    private Object restoCreatedDate;
    private Object restoUpdatedDate;
    private String restoTelephoneNum;
    private String source;

    private Button addBtn;
    private Button modifyBtn;
    private Button deleteBtn;
    private Button saveBtn;
    private Button reviewsBtn;

    // The current restaurant's id.
    private int restoId;

    private Context context;

    // A boolean to check if we can display message if the user clicks back button.
    private boolean isBackEnabled;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(CLASS_NAME, "onCreateView()");
        View v = inflater.inflate(R.layout.activity_show, container, false);

        // Get handle on all the fields of the restaurant.
        editRestoName = (EditText) v.findViewById(R.id.editRestoName);
        editRestoGenre = (EditText) v.findViewById(R.id.editRestoGenre);
        editRestoPriceRange = (EditText) v.findViewById(R.id.editRestoPriceRange);
        editRestoAddress = (EditText) v.findViewById(R.id.editRestoAddress);
        editRestoCity = (EditText) v.findViewById(R.id.editRestoCity);
        editRestoPostalCode = (EditText) v.findViewById(R.id.editRestoPostalCode);
        editRestoLatitude = (EditText) v.findViewById(R.id.editRestoLatitude);
        editRestoLongitude = (EditText) v.findViewById(R.id.editRestoLongitude);
        editCreatedDate = (EditText) v.findViewById(R.id.editCreatedDate);
        editUpdatedDate = (EditText) v.findViewById(R.id.editUpdatedDate);
        editTelephoneNumber = (EditText) v.findViewById(R.id.editTelephoneNumber);

        addBtn = (Button) v.findViewById(R.id.addBtn);
        modifyBtn = (Button) v.findViewById(R.id.modifyBtn);
        deleteBtn = (Button) v.findViewById(R.id.deleteBtn);
        saveBtn = (Button) v.findViewById(R.id.saveBtn);
        reviewsBtn = (Button) v.findViewById(R.id.reviewsBtn);

        // Get the context of current activity.
        context = getActivity();

        isBackEnabled = false;

        // Set click listener to display the reviews related to this restaurant. It will launch
        // the ReviewActivity.
        reviewsBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                displayReviews(view);
            }
        });

        // Set click listener that will to a google search for the resto's name.
        editRestoName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleSearch(view);
            }
        });

        // Set click listener that will add the current near restaurant to the database.
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addResto(view);
            }
        });

        // Set click listener that will enable the user to update the restaurant's info.
        modifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modifyResto(view);
            }
        });

        // Set click listener that will delete the restaurant from local database if it was added before.
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteResto(view);
            }
        });

        // Set click listener that will update the restaurant's info with the fields givn by the user.
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateResto(view);
            }
        });

        // Get all the informations related to the near restaurant clicked.
        Bundle portraitBundle = getActivity().getIntent().getExtras();

        restoId = -1;

        // Check if the bundle is null or not. If it is, then we get get extras from intent
        // instead. (Used for portrait mode)
        if(portraitBundle != null){
            restoName = portraitBundle.getString("restoName");
            restoGenre = portraitBundle.getString("restoGenre");
            restoPriceRange = portraitBundle.getInt("restoPriceRange");
            restoAddress = portraitBundle.getString("address");
            restoCity = portraitBundle.getString("city");
            restoPostalCode = portraitBundle.getString("postalCode");
            restoLatitude = portraitBundle.getDouble("latitude");
            restoLongitude = portraitBundle.getDouble("longitude");
            restoTelephoneNum = portraitBundle.getString("telephoneNum");
            source = portraitBundle.getString("source");

            DateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

            try {
                restoCreatedDate = format.parse(portraitBundle.getString("createdDate"));
                restoUpdatedDate = format.parse(portraitBundle.getString("updatedDate"));
            } catch (ParseException e) {
                Log.e(CLASS_NAME, "Couldn't parse date.");
                e.printStackTrace();
            }

            // Check if the restaurant was taken from heroku. If so, enable the reviews button and
            // the resto's id.
            if(source.equals("heroku"))
            {
                restoId = portraitBundle.getInt("restoid");
                reviewsBtn.setVisibility(View.VISIBLE);
            }
        }
        else {
            // Get arguments from bundle (Used for landscape mode)
            Bundle landscapeBundle = getArguments();

            restoName = landscapeBundle.getString("restoName");
            restoGenre = landscapeBundle.getString("restoGenre");
            restoPriceRange = landscapeBundle.getInt("restoPriceRange");
            restoAddress = landscapeBundle.getString("address");
            restoCity = landscapeBundle.getString("city");
            restoPostalCode = landscapeBundle.getString("postalCode");
            restoLatitude = landscapeBundle.getDouble("latitude");
            restoLongitude = landscapeBundle.getDouble("longitude");
            restoTelephoneNum = landscapeBundle.getString("telephoneNum");
            source = landscapeBundle.getString("source");

            DateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

            try {
                restoCreatedDate = format.parse(landscapeBundle.getString("createdDate"));
                restoUpdatedDate = format.parse(landscapeBundle.getString("updatedDate"));
            } catch (ParseException e) {
                Log.e(CLASS_NAME, "Couldn't parse date.");
                e.printStackTrace();
            }

            if(source.equals("heroku"))
            {
                restoId = landscapeBundle.getInt("restoid");
                reviewsBtn.setVisibility(View.VISIBLE);
            }
        }

        // Fill the fields on the views with the Restaurant's info.
        fillFields();

        // Check if the restaurant exists in the local database or not.
        searchDatabase();

        return v;
    }

    /**
     * Starts the ReviewActivity to show all reviews related to currently selected restaurant
     * onto the view.
     * @param v View
     */
    private void displayReviews(View v){
        Log.i(CLASS_NAME, "displayReviews()");
        Intent intent = new Intent();

        // Pass the resto Id you want to get reviews from.
        intent.setClass(context, ReviewActivity.class);
        intent.putExtra("restoid", restoId);

        startActivity(intent);
    }

    /**
     * Adds the currently displayed restaurant in the database with all the fields related to the
     * resto.
     * @param v View
     */
    private void addResto(View v) {
        Log.i(CLASS_NAME, "addResto()");
        DBHelper helper = new DBHelper(getActivity());

        helper.addResto(restoName, restoGenre, restoPriceRange, restoTelephoneNum, restoAddress, restoCity, restoPostalCode,
                restoLatitude, restoLongitude, "zomato", helper.getWritableDatabase());

        showMessage("Added restaurant " + restoName);
        addBtn.setVisibility(View.INVISIBLE);
        modifyBtn.setVisibility(View.VISIBLE);
        deleteBtn.setVisibility(View.VISIBLE);

        searchDatabase();
    }

    /**
     * Deletes a restaurant from the database based on its id.
     * @param v View
     */
    private void deleteResto(View v) {
        Log.i(CLASS_NAME, "deleteResto");

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);

        // Prompt user to check if they want to delete resto.
        builder.setMessage("Are you sure you want to delete?")
                .setTitle(getResources().getString(R.string.confirmation));

        // Deletes the restaurant if the user clicks OK.
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DBHelper helper = new DBHelper(context);
                helper.getWritableDatabase().delete(helper.TABLE_RESTOS, "_id = ?", new String[]{Integer.toString(restoId)});
                showMessage("Deleted restaurant");

                deleteBtn.setVisibility(View.INVISIBLE);
                modifyBtn.setVisibility(View.INVISIBLE);

                addBtn.setVisibility(View.VISIBLE);
            }


        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //dont do anything stay on this page

            }


        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Enables the user to edit all fields of the restaurant. Enable all edit texts fields.
     * @param v View
     */
    private void modifyResto(View v) {
        Log.i(CLASS_NAME, "modifyResto()");

        // If modify is pressed, we want to prompt the user if they keyback. To do that, we use this boolean.
        isBackEnabled = true;
        View view = (View) getActivity().findViewById(android.R.id.content);
        view.setFocusableInTouchMode(true);
        view.requestFocus();

        // Set the keyback click listener and prompt the user.
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(CLASS_NAME, "keyCode: " + keyCode);
                if(isBackEnabled) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                        Log.i(CLASS_NAME, "Key Back pressed");
                        cancelButtonClick();
                        return true;

                    } else
                        return false;
                }

                return false;
            }
        });

        // Enable the user to edit fields.
        enableEditTexts();

        modifyBtn.setVisibility(View.GONE);
        deleteBtn.setVisibility(View.GONE);
        saveBtn.setVisibility(View.VISIBLE);
    }


    /**
     * Runs when user clicks on the cancel button. It prompts the user for confirmation.
     */
    private void cancelButtonClick() {
        Log.i(CLASS_NAME, "cancelButtonClick");

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setMessage("Are you sure you want to leave?")
                .setTitle("Confirmation");

        // Destroys this activity if the user wants to leave.
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                getActivity().finish();

            }


        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //dont do anything stay on this page

            }


        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Does a google search for the given resto's name.
     * @param v View
     */
    private void googleSearch(View v) {
        Uri uri = Uri.parse("http://www.google.com/#q=" + restoName);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


    /**
     * Updates the resto fields with the updated fields the user submitted. It validates the fields
     * before doing any database related functions.
     * @param v
     */
    private void updateResto(View v) {
        Log.i(CLASS_NAME, "updateResto()");

        // Get all the fields values.
        restoName = editRestoName.getText().toString().trim();
        restoGenre = editRestoGenre.getText().toString().trim();
        String pricing = editRestoPriceRange.getText().toString().trim();
        restoAddress = editRestoAddress.getText().toString().trim();
        restoCity = editRestoCity.getText().toString().trim();
        restoPostalCode = editRestoPostalCode.getText().toString().trim();
        String lat = editRestoLatitude.getText().toString().trim();
        String lon = editRestoLongitude.getText().toString().trim();
        restoTelephoneNum = editTelephoneNumber.getText().toString().trim();


        // Check if all fields are valid or not.
        boolean isValid = validateFields(restoName, restoGenre, pricing + "", restoAddress, restoCity, restoPostalCode, lat, lon, restoTelephoneNum);
        if (isValid) {
            // We disable the back prompt since the user is no longer modifying the resto.
            isBackEnabled = false;

            restoLatitude = Double.parseDouble(lat);
            restoLongitude = Double.parseDouble(lon);

            DBHelper helper = new DBHelper(getActivity());
            Cursor cur = helper.getRestoInfo(restoId);

            // Update the resto.
            ContentValues cv2 = new ContentValues();
            cv2.put(helper.COL_RESTO_NAME, restoName);
            cv2.put(helper.COL_GENRE, restoGenre);
            cv2.put(helper.COL_PRICE_RANGE, pricing);
            cv2.put(helper.COL_ADDRESS, restoAddress);
            cv2.put(helper.COL_CITY, restoCity);
            cv2.put(helper.COL_POSTAL_CODE, restoPostalCode);
            cv2.put(helper.COL_LATITUDE, restoLatitude);
            cv2.put(helper.COL_LONGITUDE, restoLongitude);
            cv2.put(helper.COL_PHONE_NUMBER, restoTelephoneNum);

            helper.getWritableDatabase().update(helper.TABLE_RESTOS, cv2, "_id = ?", new String[]{Integer.toString(restoId)});

            showMessage("Updated");
            cur.close();
        } else
            showMessage("Updated Failed");

        saveBtn.setVisibility(View.GONE);
        modifyBtn.setVisibility(View.VISIBLE);
        deleteBtn.setVisibility(View.VISIBLE);

        disableEditTexts();
    }

    /**
     * Validates all the fields from the add restaurant page. It displays a message indicating
     * exactly which fields are invalid.
     *
     * @param restoName String
     * @param genre String
     * @param pricing String
     * @param address String
     * @param city String
     * @param postalCode String
     * @param latitude String
     * @param longitude String
     * @param telephoneNumber String
     * @return Boolean indicating if fields are valid or not.
     */
    private boolean validateFields(String restoName, String genre, String pricing, String address,
                                   String city, String postalCode, String latitude, String longitude, String telephoneNumber) {
        boolean isValid = true;

        // Error message to be displayed if there are any bad inputs.
        String errorMsg = "Invalid ";

        if (restoName.length() == 0) {
            Log.e(CLASS_NAME, "RestoName: Invalid value");
            errorMsg += "Resto name, ";
            isValid = false;
        }
        if (genre.length() == 0) {
            Log.e(CLASS_NAME, "Genre: Invalid value");
            errorMsg += "genre, ";
            isValid = false;
        }

        if (pricing.length() > 1 || pricing.length() == 0) {
            Log.e(CLASS_NAME, "Price range: Invalid value");
            errorMsg += "price range, ";
            isValid = false;
        }

        if (address.length() == 0) {
            Log.e(CLASS_NAME, "Address: Invalid value");
            errorMsg += "address, ";
            isValid = false;
        }
        if (city.length() == 0) {
            Log.e(CLASS_NAME, "City: Invalid value");
            errorMsg += "city, ";
            isValid = false;
        }
        if (postalCode.length() == 0) {
            Log.e(CLASS_NAME, "PostalCode: Invalid value");
            errorMsg += "postal code, ";
            isValid = false;
        }

        if (latitude.length() == 0) {
            Log.e(CLASS_NAME, "Latitude: Invalid value");
            errorMsg += "latitude, ";
            isValid = false;
        }

        if (longitude.length() == 0) {
            Log.e(CLASS_NAME, "Longitude: Invalid value");
            errorMsg += "latitude.";
            isValid = false;
        }

        if(telephoneNumber.length() == 0) {
            Log.i(CLASS_NAME, "Telephone Number: Invalid value");
            errorMsg += "telephone number, ";
            isValid = false;
        }


        // Display error message if there was an invalid field.
        if (!isValid)
            showMessage(errorMsg);

        return isValid;
    }


    /**
     * Displays all the information related to the restaurant onto the edittext fields and makes
     * them read-only.
     */
    private void fillFields() {
        // Display all the resto data on the form
        editRestoName.setText(restoName);
        editRestoGenre.setText(restoGenre);
        editRestoPriceRange.setText(restoPriceRange + "");
        editRestoAddress.setText(restoAddress);
        editRestoCity.setText(restoCity);
        editRestoPostalCode.setText(restoPostalCode);
        editRestoLatitude.setText(restoLatitude+"");
        editRestoLongitude.setText(restoLongitude+"");
        editCreatedDate.setText(restoCreatedDate + "");
        editUpdatedDate.setText(restoUpdatedDate + "");
        editTelephoneNumber.setText(restoTelephoneNum);
        disableEditTexts();
    }

    /**
     * Check if the restaurant exists in the database already or not. If so, permit the user to modify
     * or delete the restaurant from the database.
     */
    private void searchDatabase() {
        DBHelper helper = new DBHelper(getActivity());


        boolean doesExist = helper.checkRestoExists(restoName, restoGenre, restoPriceRange, restoLatitude, restoLongitude);
        //true make modify and delete buttons visible
        if (doesExist) {
            modifyBtn.setVisibility(View.VISIBLE);
            deleteBtn.setVisibility(View.VISIBLE);

            // If the resto was taken from heroku, get the resto's id.
            if(!source.equals("heroku"))
                restoId = helper.getRestoId(restoName, restoGenre, restoPriceRange, restoLatitude, restoLongitude);

        } else
            addBtn.setVisibility(View.VISIBLE);

    }

    /**
     * Displays an alert message on screen. Usually used for error or warning messages.
     *
     * @param message to display on screen.
     */
    private void showMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Disables all the edittexts and makes them read-only.
     */
    private void disableEditTexts() {
        editRestoName.setFocusable(false);
        editRestoName.setFocusableInTouchMode(false);
        editRestoName.setClickable(false);

        editRestoGenre.setFocusable(false);
        editRestoGenre.setFocusableInTouchMode(false);
        editRestoGenre.setClickable(false);

        editRestoPriceRange.setFocusable(false);
        editRestoPriceRange.setFocusableInTouchMode(false);
        editRestoPriceRange.setClickable(false);

        editRestoAddress.setFocusable(false);
        editRestoAddress.setFocusableInTouchMode(false);
        editRestoAddress.setClickable(false);

        editRestoCity.setFocusable(false);
        editRestoCity.setFocusableInTouchMode(false);
        editRestoCity.setClickable(false);

        editRestoPostalCode.setFocusable(false);
        editRestoPostalCode.setFocusableInTouchMode(false);
        editRestoPostalCode.setClickable(false);

        editRestoLatitude.setFocusable(false);
        editRestoLatitude.setFocusableInTouchMode(false);
        editRestoLatitude.setClickable(false);

        editRestoLongitude.setFocusable(false);
        editRestoLongitude.setFocusableInTouchMode(false);
        editRestoLongitude.setClickable(false);

        editTelephoneNumber.setFocusable(false);
        editTelephoneNumber.setFocusableInTouchMode(false);
        editTelephoneNumber.setClickable(false);
    }

    /**
     * Enables all the edittexts and makes them editable.
     */
    private void enableEditTexts() {
        editRestoName.setFocusable(true);
        editRestoName.setFocusableInTouchMode(true);
        editRestoName.setClickable(true);

        editRestoGenre.setFocusable(true);
        editRestoGenre.setFocusableInTouchMode(true);
        editRestoGenre.setClickable(true);

        editRestoPriceRange.setFocusable(true);
        editRestoPriceRange.setFocusableInTouchMode(true);
        editRestoPriceRange.setClickable(true);

        editRestoAddress.setFocusable(true);
        editRestoAddress.setFocusableInTouchMode(true);
        editRestoAddress.setClickable(true);

        editRestoCity.setFocusable(true);
        editRestoCity.setFocusableInTouchMode(true);
        editRestoCity.setClickable(true);

        editRestoPostalCode.setFocusable(true);
        editRestoPostalCode.setFocusableInTouchMode(true);
        editRestoPostalCode.setClickable(true);

        editRestoLatitude.setFocusable(true);
        editRestoLatitude.setFocusableInTouchMode(true);
        editRestoLatitude.setClickable(true);

        editRestoLongitude.setFocusable(true);
        editRestoLongitude.setFocusableInTouchMode(true);
        editRestoLongitude.setClickable(true);

        editTelephoneNumber.setFocusable(true);
        editTelephoneNumber.setFocusableInTouchMode(true);
        editTelephoneNumber.setClickable(true);
    }
}

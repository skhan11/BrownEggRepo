package com.restobrownegg.bean;

import java.util.Date;

/**
 * This class is a bean that holds all information about a restaurant. This restaurant object is mainly
 * used for data exchange between the android app, Zomato and/or Heroku. This bean will hold all
 * information temporarily in order to later display the data onto a ListView.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class Restaurant {
    private int restoId;
    private int restoPriceRange;

    private double longitude;
    private double latitude;

    private Date createdDate;
    private Date updatedDate;

    private String restoName;
    private String restoGenre;
    private String address;
    private String city;
    private String postalCode;
    private String telephoneNumber;
    private String source;

    /**
     * Default empty constructor.
     */
    public Restaurant() {
    }

    /**
     * Return the resto's id.
     * @return restoId int
     */
    public int getRestoId() {
        return restoId;
    }

    /**
     * Return the address.
     * @return address String
     */
    public String getAddress() {
        return address;
    }

    /**
     * Return the city.
     * @return city String
     */
    public String getCity() {
        return city;
    }

    /**
     * Return the createdDate.
     * @return createdDate Date
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Return the latitude.
     * @return latitude double
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Return the longitude.
     * @return longitude double
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Return the postalCode.
     * @return postalCode String
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Return the resto's Genre.
     * @return restoGenre String
     */
    public String getRestoGenre() {
        return restoGenre;
    }

    /**
     * Return the resto's Name.
     * @return restoName String
     */
    public String getRestoName() {
        return restoName;
    }

    /**
     * Return the resto's PriceRange.
     * @return restoPriceRange int
     */
    public int getRestoPriceRange() {
        return restoPriceRange;
    }

    /**
     * Return the resto's telephoneNumber.
     * @return telephoneNumber String
     */
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * Return the source from where we got this restaurant.
     * @return source String
     */
    public String getSource() {
        return source;
    }

    /**
     * Return the updatedDate.
     * @return updatedDate Date
     */
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Set the resto's id.
     * @param restoId int
     */
    public void setRestoId(int restoId) {
        this.restoId = restoId;
    }

    /**
     * Set the resto's address.
     * @param address String
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Set the resto's city
     * @param city String
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Set the createdDate
     * @param createdDate Date
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Set the latitude
     * @param latitude double
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * Set the longitude
     * @param longitude double
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * Set the resto's postalcode
     * @param postalCode String
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Set the resto's genre
     * @param restoGenre String
     */
    public void setRestoGenre(String restoGenre) {
        this.restoGenre = restoGenre;
    }

    /**
     * Set the resto's name
     * @param restoName String
     */
    public void setRestoName(String restoName) {
        this.restoName = restoName;
    }

    /**
     * Set the resto's priceRange
     * @param restoPriceRange int
     */
    public void setRestoPriceRange(int restoPriceRange) {
        this.restoPriceRange = restoPriceRange;
    }

    /**
     * Set the source
     * @param source String
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Set the resto's phone number
     * @param telephoneNumber String
     */
    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    /**
     * Set the updated date
     * @param updatedDate Date
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}


package com.restobrownegg.bean;

/**
 * A bean to hold information about a Review. This is used to exchange data between
 * the database (heroku) and the view and to display this informations.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class Review {
    private String title;
    private String content;
    private String email;

    private int rating;
    private int restoId;

    /**
     * Get the review's title
     * @return title String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the review's title
     * @param title String
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get the review's rating
     * @return rating int
     */
    public int getRating() {
        return rating;
    }

    /**
     * Set the review's rating
     * @param rating int
     */
    public void setRating(int rating) {
        this.rating = rating;
    }

    /**
     * Get the review's content
     * @return content String
     */
    public String getContent() {
        return content;
    }

    /**
     * Set the review's content
     * @param content String
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Get the reviewer's email
     * @return email String
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the reviewer's email
     * @param  email String
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the review's restoId
     * @return restoId String
     */
    public int getRestoId() {
        return restoId;
    }

    /**
     * Set the review's restoId
     * @param restoId String
     */
    public void setRestoId(String restoId) {
        this.restoId = this.restoId;
    }
}

package com.restobrownegg.home;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;

import com.restobrownegg.placepicker.PlacePickerFragment;
import com.restobrownegg.add.RestoAddFragment;
import com.restobrownegg.search.RestoSearchFragment;
import com.restobrownegg.tipcalculator.TipCalculatorFragment;

/**
 * This class will be launching the home menu items as an activity instead of a fragment. This is
 * used for when the screen is in portrait mode. Therefore, if it detects that the screen is in
 * landscape mode, it will destroy itself.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class HomeDetailsActivity extends MenuActivity {

    private final String CLASS_NAME = "HomeDetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(CLASS_NAME,"onCreate()");

        // Check if the screen is in landscape mode. If it is, we don't need this activity.
        // Therefore, we call the finish() method.
        if(getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE){
            finish();
            return;
        }

        if(savedInstanceState == null){

            Bundle b = getIntent().getExtras();
            int index = b.getInt("curPosition");

            // Open the appropriate fragment by checking the index.
            Fragment newFragment =null;
            FragmentManager fm = getSupportFragmentManager();
            switch(index){
                case 1:
                    Log.i(CLASS_NAME,"Loading RestoSearchFragment");
                    newFragment = new RestoSearchFragment();
                    break;
                case 3:
                    Log.i(CLASS_NAME,"Loading TipCalculatorFragment");
                    newFragment = new TipCalculatorFragment();
                    break;
                case 4:
                    Log.i(CLASS_NAME,"Loading RestoFragment");
                    newFragment = new RestoAddFragment();
                    break;
                case 5:
                    Log.i(CLASS_NAME,"Loading PlacePickerFragment");
                    newFragment = new PlacePickerFragment();
                    break;
            }

            // Use android.R.id.content to get the parent layout and open as activity.
            newFragment.setArguments(getIntent().getExtras());
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(android.R.id.content, newFragment);
            ft.commit();
        }
    }
}

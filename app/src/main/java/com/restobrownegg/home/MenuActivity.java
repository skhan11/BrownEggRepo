package com.restobrownegg.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.restobrownegg.AboutActivity;
import com.restobrownegg.R;
import com.restobrownegg.tasks.SyncTask;
import com.restobrownegg.dao.DBHelper;
import com.restobrownegg.signin.SignUpActivity;

import java.util.ArrayList;

/**
 * This class will take care of the different events that will be launched in menu.
 * There are three different menu items About, Dawson, Settings.
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 *
 */
public class MenuActivity extends AppCompatActivity {

    private final String CLASS_NAME = "MenuActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_home);
    }

    /**
     *  Create options menu
     * @param menu menu object
     * @return boolean if inflated
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    /**
     *
     * There are three different item menus. The first one launches an about page.
     * The second one launches the dawson comp sci web page and the last one launches the settings
     * page where user can input new data.
     * @param item menu item selected
     * @return true if selected false if nothing happens
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //id of selected item for intent launch of the about screen
        //launches the about page
        if (id == R.id.aboutMenuItem) {
            Log.i(CLASS_NAME,"Clicked about menu item");
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        }

        //launches the dawson comp sci web page
        if (id == R.id.dawsonMenuItem) {
            Log.i(CLASS_NAME,"Clicked dawson menu item");
            String url = "https://www.dawsoncollege.qc.ca/computer-science-technology/";
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(i);
        }

        // launches the sign up form
        if (id == R.id.settingsMenuItem)
        {
            Log.i(CLASS_NAME,"Clicked settings menu item");
            Intent intent = new Intent(this,SignUpActivity.class);
            intent.putExtra("firstTime", false);
            startActivity(intent);
        }

        //https://developer.android.com/training/basics/network-ops/connecting.html
        if(id == R.id.syncBackEndItem) {
            Log.i(CLASS_NAME,"Syncing to heroku...");
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);


            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected())
            {


                SharedPreferences prefs = getSharedPreferences("userInfo",MODE_PRIVATE);

                DBHelper helper = new DBHelper(this);

                Cursor cursor = helper.getFavouriteRestos(prefs.getString("email",""));
                ArrayList<String> jsonObjectArrayList = new ArrayList<>();
                int count = cursor.getCount();
                String jsonData;
                //https://github.com/Android518-2016/week14-net-HttpURLConnection-POST/blob/master/app/src/main/java/ca/campbell/httpexamplepost/HttpsExamplePOST.java
                cursor.moveToFirst();
                while(count!=0)
                {
                    Log.i(CLASS_NAME,prefs.getString("email",""));
                    Log.i(CLASS_NAME, prefs.getString("password",""));
                    Log.i(CLASS_NAME, cursor.getString(cursor.getColumnIndex(helper.COL_RESTO_NAME)));

                    jsonData = "{ \"email\":  \"" + prefs.getString("email","") + "\", "
                            + " \"password\":  \"" + prefs.getString("password","")+ "\", "
                            + " \"name\":  \"" + cursor.getString(cursor.getColumnIndex(helper.COL_RESTO_NAME)) + "\", "
                            + " \"genre\":  \"" + cursor.getString(cursor.getColumnIndex(helper.COL_GENRE)) + "\", "
                            + " \"pricing\":  \"" + cursor.getString(cursor.getColumnIndex(helper.COL_PRICE_RANGE)) + "\", "
                            + " \"address\":  \"" + cursor.getString(cursor.getColumnIndex(helper.COL_ADDRESS)) + "\", "
                            + " \"latitude\":  \"" + cursor.getString(cursor.getColumnIndex(helper.COL_LATITUDE)) + "\", "
                            + " \"longitude\":  \"" + cursor.getString(cursor.getColumnIndex(helper.COL_LONGITUDE))+ "\" }";

                    count--;
                    cursor.moveToNext();
                    Log.i(CLASS_NAME, count+"");

                    Log.d(CLASS_NAME, jsonData.toString());

                    jsonObjectArrayList.add(jsonData);


                }
                if(jsonObjectArrayList.size() != 0) {
                    SyncTask task = new SyncTask(jsonObjectArrayList);
                    task.execute("https://young-shelf-72681.herokuapp.com/api/apiv1/addresto");
                }
                //make a dialog
                else
                    Log.i(CLASS_NAME,"list is empty no favourites");



            }
            else
                Log.e(CLASS_NAME, "Failed to sync to heroku");


        }

        return false;
    }






}

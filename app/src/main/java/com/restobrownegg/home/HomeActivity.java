package com.restobrownegg.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.restobrownegg.AboutActivity;
import com.restobrownegg.near.NearRestoActivity;
import com.restobrownegg.placepicker.PlacePickerFragment;
import com.restobrownegg.R;
import com.restobrownegg.add.RestoAddFragment;
import com.restobrownegg.search.RestoSearchFragment;
import com.restobrownegg.tipcalculator.TipCalculatorFragment;
import com.restobrownegg.favourite.FavouriteRestosActivity;

/**
 * This class is the curPosition page of the app where all the different resto events are listed.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class HomeActivity extends MenuActivity {

    private final String CLASS_NAME = "HomeActivity";

    // Integer representation of each program.
    private final int FAVOURITE_RESTO = 0;
    private final int SEARCH_RESTO = 1;
    private final int NEAR_RESTO = 2;
    private final int TIP_CALCULATOR = 3;
    private final int RESTO_ADD = 4;
    private final int PLACE_PICKER = 5;

    // Used for state keeping.
    private boolean mDualPane = true;
    private int curPosition;

    private ImageButton favouriteBtn;
    private ImageButton searchBtn;
    private ImageButton nearBtn;
    private ImageButton tipBtn;
    private ImageButton restoAddBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.setTitle(getResources().getString(R.string.app_label));
        Log.i(CLASS_NAME, "onCreate()");

        favouriteBtn = (ImageButton) findViewById(R.id.imgBtnFav);
        searchBtn = (ImageButton) findViewById(R.id.imgBtnSearch);
        nearBtn = (ImageButton) findViewById(R.id.imgBtnNear);
        tipBtn = (ImageButton) findViewById(R.id.imgBtnTip);
        restoAddBtn = (ImageButton) findViewById(R.id.imgBtnAdd);


        // Check if there was a previous state. If so, reload the selection.
        if (savedInstanceState != null && savedInstanceState.getInt("curPosition", 0) != 0) {
            curPosition = savedInstanceState.getInt("curPosition", 0);
        }

        View detailsFrame = findViewById(R.id.details);

        // Check if we can display the frame layout.
        mDualPane = detailsFrame != null
                && detailsFrame.getVisibility() == View.VISIBLE;

        // Check which button was previously clicked.
        if (mDualPane) {
            switch (curPosition) {
                case SEARCH_RESTO:
                    searchBtn.performClick();
                    break;
                case NEAR_RESTO:
                    nearBtn.performClick();
                    break;
                case TIP_CALCULATOR:
                    tipBtn.performClick();
                    break;
                case RESTO_ADD:
                    restoAddBtn.performClick();
                    break;
            }
        }

        // Get the user's info and display a welcome message.
        SharedPreferences prefs = getSharedPreferences("userInfo", MODE_PRIVATE);

        String lastName = prefs.getString("lastName", "");
        String firstName = prefs.getString("firstName", "");

        TextView footer = (TextView) findViewById(R.id.welcomeText);
        footer.setText("Welcome " + lastName + " " + firstName);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current selection to keep state.
        outState.putInt("curPosition", curPosition);
    }

    /**
     * Open the FavouriteRestosActivity when its button is clicked.
     *
     * @param v
     */
    public void favouriteRestosClick(View v) {
        Log.i(CLASS_NAME, "FavoriteRestos Clicked");
        curPosition = FAVOURITE_RESTO;

        Intent intent = new Intent();

        intent.setClass(this, FavouriteRestosActivity.class);

        startActivity(intent);
    }

    /**
     * Open the RestoSearchFragment when its button is clicked.
     * It checks whether to display landscape or portrait.
     *
     * @param v
     */
    public void restoSearchClick(View v) {
        Log.i(CLASS_NAME, "RestoSearch Clicked");

        if (mDualPane && curPosition != SEARCH_RESTO) {
            // Open a fragment for portrait mode.
            openRestoSearch();
        } else {
            // Open an activity for portrait mode.
            curPosition = SEARCH_RESTO;
            Intent intent = new Intent();

            intent.setClass(this, HomeDetailsActivity.class);

            intent.putExtra("curPosition", SEARCH_RESTO);

            startActivity(intent);
        }
    }

    /**
     * Opens the RestoSearchFragment in the frame layout for landscape mode.
     */
    private void openRestoSearch() {
        curPosition = SEARCH_RESTO;
        Fragment newFragment = new RestoSearchFragment();

        FragmentManager fm = getSupportFragmentManager();

        // Replace existing fragment.
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.details, newFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null).commit();
    }

    /**
     * Open the NearRestoActivity when its button is clicked.
     *
     * @param v
     */
    public void nearRestoClick(View v) {
        Log.i(CLASS_NAME, "NearRestos Clicked");
        curPosition = NEAR_RESTO;

        Intent intent = new Intent();

        intent.setClass(this, NearRestoActivity.class);

        startActivity(intent);
    }

    /**
     * Open the TipCalculatorFragment when its button is clicked.
     * It checks whether to display landscape or portrait.
     *
     * @param v
     */
    public void tipCalculatorClick(View v) {
        Log.i(CLASS_NAME, "TipCalculator Clicked");

        if (mDualPane && curPosition != TIP_CALCULATOR) {
            // Open a fragment for portrait mode.
            openTipCalculator();
        } else {
            // Open an activity for portrait mode.
            curPosition = TIP_CALCULATOR;
            Intent intent = new Intent();

            intent.setClass(this, HomeDetailsActivity.class);

            intent.putExtra("curPosition", TIP_CALCULATOR);

            startActivity(intent);
        }
    }

    /**
     * Opens the TipCalculatorFragment in the frame layout for landscape mode.
     */
    private void openTipCalculator() {
        curPosition = TIP_CALCULATOR;
        Fragment newFragment = new TipCalculatorFragment();

        FragmentManager fm = getSupportFragmentManager();

        // Replace existing fragment.
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.details, newFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null).commit();
    }

    /**
     * Open the RestoAddFragment when its button is clicked.
     * It checks whether to display landscape or portrait.
     *
     * @param v
     */
    public void restoAddClick(View v) {
        Log.i(CLASS_NAME, "RestoAdd Clicked");

        if (mDualPane && curPosition != RESTO_ADD) {
            // Open a fragment for portrait mode.
            openRestoAdd();
        } else {
            // Open an activity for portrait mode.
            curPosition = RESTO_ADD;
            Intent intent = new Intent();

            intent.setClass(this, HomeDetailsActivity.class);

            intent.putExtra("curPosition", RESTO_ADD);

            startActivity(intent);
        }
    }

    /**
     * Opens the RestoAddFragment in the frame layout for landscape mode.
     */
    private void openRestoAdd() {
        curPosition = RESTO_ADD;
        Fragment newFragment = new RestoAddFragment();

        FragmentManager fm = getSupportFragmentManager();

        // Replace existing fragment.
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.details, newFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null).commit();
    }

    public void placePickerClick(View v)
    {
        Log.i(CLASS_NAME,"PlacePicker Clicked");

        if(mDualPane && curPosition != PLACE_PICKER)
            openPlacePicker();
        else
        {
            curPosition = PLACE_PICKER;
            Intent intent = new Intent();

            intent.setClass(this, HomeDetailsActivity.class);

            intent.putExtra("curPosition", PLACE_PICKER);

            startActivity(intent);

        }
    }

    /**
     * Opens the PlacePickerFragment in the frame layout for landscape mode.
     */
    private void openPlacePicker() {
        curPosition = PLACE_PICKER;
        Fragment newFragment = new PlacePickerFragment();

        FragmentManager fm = getSupportFragmentManager();

        // Replace existing fragment.
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.details, newFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null).commit();
    }

    /**
     * When the logo img is clicked, open the AboutActivity.
     *
     * @param v
     */
    public void logoClick(View v) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
}

package com.restobrownegg;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.restobrownegg.home.MenuActivity;

/**
 * This class takes care of the different activities in the about page.
 * The about page has 7 events in total. The first event is when you click on Dawson click which
 * will launch a web page. The second event is when you click on the course id which will launch
 * the Dawson Comp Sci web page. The last 5 events occur when you click on the images of each
 * individual and it will display their respective blurbs.
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class AboutActivity extends MenuActivity {

    private final String CLASS_NAME = "AboutActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Log.i(CLASS_NAME, "onCreate()");
    }


    /**
     * This method takes care of launching the dawson web page when the title is clicked.
     * @param v View
     */
    public void displayDawsonWebsite(View v)
    {
        Log.i(CLASS_NAME, "Displaying Webpage");
        String url = "https://www.dawsoncollege.qc.ca/";
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(i);

    }

    /**
     * This method takes care of launching the dawson comp sci page when the course id is clicked.
     * @param v View
     */
    public void displayCompSci(View v)
    {
        Log.i(CLASS_NAME, "Displaying CompSci");
        String url = "https://www.dawsoncollege.qc.ca/computer-science-technology/";
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(i);
    }

    /**
     * This method will display the blurb of Naasir when his image is clicked.
     * @param v View
     */
    public void displayNasBlurb(View v)
    {

        Log.i(CLASS_NAME, "Displaying nas Blurb");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.naasir));
        builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * This method will display the blurb of Shifat when his image is clicked.
     * @param v View
     */
    public void displayShifatBlurb(View v)
    {

        Log.i(CLASS_NAME, "Displaying shift Blurb");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.shifat));
        builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * This method will display the blurb of Salman when his image is clicked.
     * @param v View
     */
    public void displaySalmanBlurb(View v)
    {

        Log.i(CLASS_NAME, "Displaying salman Blurb");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.salman));
        builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * This method will display the blurb of Sam when his image is clicked.
     * @param v View
     */
    public void displaySamBlurb(View v)
    {

        Log.i(CLASS_NAME, "Displaying sam Blurb");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.seaim));
        builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * This image will display the blurb of William when his image is clicked.
     * @param v
     */
    public void displayWillyBlurb(View v)
    {

        Log.i(CLASS_NAME, "Displaying willy Blurb");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.william));
        builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }






}

package com.restobrownegg.search;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.restobrownegg.R;
import com.restobrownegg.dao.DBHelper;
import com.restobrownegg.details.RestoDetailsActivity;
import com.restobrownegg.details.RestoDetailsFragment;

/**
 * This class will be taking care of displaying the results of restaurants found by searching the database
 * for matching fields compared to what the user gave in the RestoSearchFragment. It checks for both
 * the keyword in the resto's name and the resto's genre.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class RestoSearchResultsActivity extends AppCompatActivity {

    private final String CLASS_NAME = "RestoSeResultsActivity";

    private static DBHelper dbh;

    private SimpleCursorAdapter sca;
    private Cursor cursor;

    private String keyword;
    private String genre;

    // The selected restaurant from the list.
    private int selection = 0;

    private boolean mDualPane = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resto_search_results);
        Log.i(CLASS_NAME, "onCreate()");

        // Check if a previous list item was selected. If so, restore the selection in order to restore
        // the previous state.
        if (savedInstanceState != null && savedInstanceState.getInt("selection", 0) != 0) {
            selection = savedInstanceState.getInt("selection", 0);
        }

        // Get the fields in order to search through the database.
        keyword = getIntent().getStringExtra("keyword");
        genre = getIntent().getStringExtra("genre");

        // Define what we want to retrieve from the database.
        String[] from = {DBHelper.COL_RESTO_NAME, DBHelper.COL_GENRE, DBHelper.COL_ID};

        // Define where the columns will be displayed.
        int[] to = {R.id.restoName, R.id.restoGenre, R.id.restoId};

        dbh = DBHelper.getDBHelper(this);

        ListView lv = (ListView) findViewById(R.id.restoFoundList);

        cursor = dbh.searchRestos(keyword, genre);
        sca = new SimpleCursorAdapter(this, R.layout.resto_list, cursor, from, to, 0);

        Log.i(CLASS_NAME, "Restos found: " + cursor.getCount());

        lv.setAdapter(sca);
        lv.setOnItemClickListener(onClick);

        View detailsFrame = findViewById(R.id.restoDetailsFrameLayout);

        // Check if we can display the fragment in the frame layout (for landscape mode).
        mDualPane = detailsFrame != null
                && detailsFrame.getVisibility() == View.VISIBLE;

        if (mDualPane && cursor != null && cursor.getCount() != 0) {
            // Simulate an onclick for the item.
            // As shown by Ed. S from stackoverflow (first answer from the forum):
            // - http://stackoverflow.com/questions/8611612/how-to-trigger-onlistitemclick-in-a-listfragment
            lv.requestFocusFromTouch();
            lv.setSelection(selection);
            lv.performItemClick(lv.getAdapter().getView(selection, null, null), selection, selection);

        } else {
            lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            lv.setItemChecked(selection, true);
        }
    }

    /**
     * On item click listener that will get the restoId and open the details for that resto by
     * calling the showRestoDetails method and passing it the appropriate values.
     */
    private AdapterView.OnItemClickListener onClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // For logging purposes.
            TextView restoNameTextView = (TextView) view.findViewById(R.id.restoName);
            String restoName = restoNameTextView.getText().toString();
            Log.i(CLASS_NAME, "Resto clicked: " + restoName);

            // Get the item view that is selected in order to retrieve the resto's id.
            TextView restoIdTextView = (TextView) view.findViewById(R.id.restoId);
            int restoId = Integer.parseInt(restoIdTextView.getText().toString());

            showRestoDetails(position, restoId);
        }
    };

    /**
     * Launch the resto details fragment/activity from the given resto id. The resto's id will
     * determine which restaurant you want to display.
     *
     * @param index int
     * @param restoId int
     */
    private void showRestoDetails(int index, int restoId) {
        // Update current selection.
        selection = index;

        // Check if landscape mode or not. If so, load the resto details as a fragment in the FrameLayout.
        if (mDualPane) {
            RestoDetailsFragment details = (RestoDetailsFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.restoDetailsFrameLayout);

            // Check if there's any resto details displaying in the frame layout or not. If so,
            // check if the user is trying to open the same restaurant again by checking the
            // shownIndex.
            if (details == null || details.getShownIndex() != index) {
                // Show new restaurant selection
                details = RestoDetailsFragment.newInstance(index);

                // Pass in the restoId so we can later search through the database.
                Bundle args = new Bundle();
                args.putInt("restoId", restoId);
                args.putInt("index", index);
                details.setArguments(args);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.restoDetailsFrameLayout, details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null).commit();
            }// End inner if.
        }// End outer if.
        else {
            // Start the RestoDetailsActivity since we know the screen is in portrait mode.
            Intent intent = new Intent();

            intent.setClass(this, RestoDetailsActivity.class);

            // Put in the current index.
            intent.putExtra("index", index);
            intent.putExtra("restoId", restoId);

            // Start the RestoDetailsActivity activity.
            startActivity(intent);
        }// End if - else.
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current selection to restore state.
        outState.putInt("selection", selection);
    }
}

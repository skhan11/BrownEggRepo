package com.restobrownegg.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.restobrownegg.R;

import java.util.Arrays;


/**
 * This class will be taking care of displaying the search bar and launching an activity containing
 * the results in a listview format. The Search will use both the resto's name and its genre to query
 * the database. If the user decides to leave the 'keyword' field empty, it will still work and get
 * all restos in that genre.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class RestoSearchFragment extends Fragment {

    private final String CLASS_NAME = "RestoSearchFragment";

    private Button searchBtn;
    private EditText keywordEditText;
    private Spinner genreSpinner;

    // Array used to populate the genreSpinner.
    private String [] genreArray;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(CLASS_NAME, "onCreate()");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(CLASS_NAME, "onCreateView()");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_resto_search, container, false);

        // Get a handle on the widgets.
        searchBtn = (Button) v.findViewById(R.id.searchBtn);
        keywordEditText = (EditText) v.findViewById(R.id.searchEditText);
        genreSpinner = (Spinner) v.findViewById(R.id.spinnerGenre);

        genreArray = getResources().getStringArray(R.array.genres);

        // ArrayAdapter to populate the genre spinner by loading values from arrays.xml.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item,  Arrays.asList(genreArray));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genreSpinner.setAdapter(adapter);

        // Set a click listener for the search button.
        searchBtn.setOnClickListener(onClick);

        return v;
    }

    /**
     * Gets the keyword from within the edittext and sends it to the RestoSearchResultsActivity
     * to display results in a listview format.
     */
    private View.OnClickListener onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();

            intent.setClass(getActivity(), RestoSearchResultsActivity.class);

            // Send search fields.
            intent.putExtra("keyword", keywordEditText.getText().toString());
            intent.putExtra("genre", genreSpinner.getSelectedItem().toString());

            startActivity(intent);
        }
    };

}

package com.restobrownegg.add;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.restobrownegg.R;
import com.restobrownegg.dao.DBHelper;

import java.util.Arrays;

/**
 * This Fragment will be taking care of storing a new Restaurant in the database from user input.
 * There's also some input validation going on before doing any database related functions. Since
 * our favourites database is seperate, the user will have to later add the restaurant added to
 * the database to its favourites.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */

public class RestoAddFragment extends Fragment{

    private final String CLASS_NAME = "RestoAddFragment";

    private Spinner spinnerGenre;
    private Spinner spinnerPricing;

    private EditText editRestoName;
    private EditText editPhoneNumber;
    private EditText editAddress;
    private EditText editCity;
    private EditText editPostalCode;
    private EditText editLatitude;
    private EditText editLongitude;

    // Arrays to populate the spinners.
    private String [] pricings;
    private String [] genres;

    private Button cancelBtn;
    private Button saveBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        Log.i(CLASS_NAME, "onCreateView()");

        View v = inflater.inflate(R.layout.fragment_resto_add, container, false);

        // Get a handle on all the edit text fields so we can later get the input values.
        editRestoName = (EditText) v.findViewById(R.id.editRestoName);
        spinnerGenre = (Spinner) v.findViewById(R.id.spinnerGenre);
        spinnerPricing = (Spinner) v.findViewById(R.id.spinnerPricing);
        editPhoneNumber = (EditText) v.findViewById(R.id.editPhoneNumber);
        editAddress = (EditText) v.findViewById(R.id.editAddress);
        editCity = (EditText) v.findViewById(R.id.editCity);
        editPostalCode = (EditText) v.findViewById(R.id.editPostalCode);
        editLatitude = (EditText) v.findViewById(R.id.editLatitude);
        editLongitude = (EditText) v.findViewById(R.id.editLongitude);

        // Get the array values from resources.
        pricings = getResources().getStringArray(R.array.pricing);
        genres = getResources().getStringArray(R.array.genres);

        // ArrayAdapter to populate the spinner for pricing.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item,  Arrays.asList(pricings));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPricing.setAdapter(adapter);

        // ArrayAdapter to populate the spinner for genres.
        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item,  Arrays.asList(genres));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGenre.setAdapter(adapter);

        // Set on click listeners for both buttons.
        cancelBtn = (Button) v.findViewById(R.id.cancelBtn);
        saveBtn = (Button) v.findViewById(R.id.saveBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelButtonClick();
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveButtonClick();
            }
        });

        // Checks for when the back button is clicked. Special thanks to Mark Allison and cV2:
        // - http://stackoverflow.com/questions/7992216/android-fragment-handle-back-button-press
        v.setFocusableInTouchMode(true);
        v.requestFocus();

        // We want the back button to be show an alert when the screen is in portrait mode.
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            v.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    Log.i(CLASS_NAME, "keyCode: " + keyCode);
                    if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP ) {
                        Log.i(CLASS_NAME, "onKey Back listener");
                        cancelButtonClick();
                        return true;
                    }
                    else
                        return false;
                }
            });
        }

        Log.i(CLASS_NAME, "finished onCreateView()");
        return v;
    }

    /**
     * This method will be run when the save button is clicked. It will then proceed to get all values
     * from the edit text fields and validate them. If the fields are valid, it will insert that restaurant
     * into the database with the DBHelper. It will also check if the location is already taken since
     * two restaurants can't be in the same location.
     */
    public void saveButtonClick()
    {
        Log.i(CLASS_NAME, "saveButtonClick()");
        // Get all the field values from the view.
        int pricing = Integer.parseInt(spinnerPricing.getSelectedItem().toString());
        String restoName = editRestoName.getText().toString().trim();
        String genre = spinnerGenre.getSelectedItem().toString();
        String phoneNumber = editPhoneNumber.getText().toString().trim();
        String address = editAddress.getText().toString().trim();
        String city = editCity.getText().toString().trim();
        String postalCode = editPostalCode.getText().toString().trim();
        String latitude = editLatitude.getText().toString().trim();
        String longitude = editLongitude.getText().toString().trim();

        // Check if the fields are valid or not. If it is, proceed to insert resto into the database.
        boolean isValid = validateFields(restoName,genre,pricing,phoneNumber,address,city,postalCode,
                latitude,longitude);
        if(isValid) {
            double lat = Double.parseDouble(latitude);
            double lon = Double.parseDouble(longitude);

            DBHelper helper = new DBHelper(getActivity());

            // Check if the location is already taken or not in the database.
            SQLiteDatabase db = helper.getWritableDatabase();
            Cursor cur = db.query(DBHelper.TABLE_RESTOS,null,"city=? AND address = ? AND postal_code = ?", new String[]{city,address,postalCode},null,null,null);

            // If location is taken, display error message.
            if(cur.getCount() != 0)
                alertMessages("Location already exists");
            else {
                // Store the restaurant in the database.
                helper.addResto(restoName, genre, pricing, phoneNumber, address, city, postalCode,lat,lon,"direct input",db);

                alertMessages("Restaurant "+restoName+" successfully saved.");
            }// End inner if-else.
        }// End outer if.
    }

    /**
     * Runs when user wants to exit. It prompts the user for confirmation before running the exit
     * code.
     */
    public void cancelButtonClick()
    {
        Log.i(CLASS_NAME,"cancelButtonClick()");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Are you sure you want to leave?")
                .setTitle("Confirmation");

        // Destroys this activity if the user wants to leave.
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                getActivity().finish();

            }


        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int id)
            {
                //dont do anything, stay on this page

            }


        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Displays a prompt to the user in order to display a message. Usually used for warnings and/or
     * errors.
     * @param msg to display
     */
    private void alertMessages(String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    /**
     * Validates all the fields from the add restaurant page. It displays a message indicating
     * exactly which fields are invalid.
     *
     * @param restoName String
     * @param genre String
     * @param pricing int
     * @param phoneNumber String
     * @param address String
     * @param city String
     * @param postalCode String
     * @param latitude String
     * @return A boolean indicating if the fields are valid or not.
     */
    private boolean validateFields(String restoName,String genre,int pricing,String phoneNumber,String address,String city,String postalCode, String latitude, String longitude)
    {
        boolean isValid = true;

        // Error message to be displayed if there are any bad inputs.
        String errorMsg = "Invalid ";

        if(restoName == null || restoName.length()== 0) {
            Log.i(CLASS_NAME, "RestoName: Invalid value");
            errorMsg += "Resto name, ";
            isValid = false;
        }
        if (genre == null || genre.length() == 0) {
            Log.i(CLASS_NAME, "Genre: Invalid value");
            errorMsg += "genre, ";
            isValid = false;
        }
        if(!(pricing == 1 || pricing == 2 ||pricing == 3 ||pricing == 4 ||pricing == 5)) {
            Log.i(CLASS_NAME, "Price range: Invalid value");
            errorMsg += "price range, ";
            isValid = false;
        }
        if(phoneNumber == null || phoneNumber.length() <= 0) {
            Log.i(CLASS_NAME, "Phone number: Invalid value");
            errorMsg += "phone #, ";
            isValid = false;
        }
        if(address == null || address.length() == 0) {
            Log.i(CLASS_NAME, "Address: Invalid value");
            errorMsg += "address, ";
            isValid = false;
        }
        if(city == null || city.length() == 0) {
            Log.i(CLASS_NAME, "City: Invalid value");
            errorMsg += "city, ";
            isValid = false;
        }
        if(postalCode == null || postalCode.length() == 0) {
            Log.i(CLASS_NAME, "PostalCode: Invalid value");
            errorMsg += "postal code, ";
            isValid = false;
        }

        try{
            Double.parseDouble(latitude);
        }catch (Exception e){
            Log.i(CLASS_NAME, "Latitude: Invalid value");
            errorMsg += "latitude, ";
            isValid = false;
        }

        try{
            Double.parseDouble(longitude);
        }catch (Exception e){
            Log.i(CLASS_NAME, "Longitude: Invalid value");
            errorMsg += "longitude. ";
            isValid = false;
        }

        // Display error message if there was an invalid field.
        if(!isValid)
            alertMessages(errorMsg);

        return isValid;
    }
}

package com.restobrownegg.near;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.restobrownegg.GPSTracker;
import com.restobrownegg.R;
import com.restobrownegg.tasks.ZomatoTask;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * This class will be taking care of getting the current location using the GPS and fetch nearby
 * restaurants. The restaurants will be used to populate the listview and open resto details.
 *
 * Special thanks to Ravi Tamada for the Android GPS tutorial that was posted here:
 * - http://www.androidhive.info/2012/07/android-gps-location-manager-tutorial/
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class NearRestoActivity extends FragmentActivity {

    private final String CLASS_NAME = "NearRestoActivity";

    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_resto);

        Log.i(CLASS_NAME, "onCreate()");

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        try {
            if (networkInfo != null && networkInfo.isConnected()) {
                // Create class object
                gps = new GPSTracker(this);

                // Check if GPS enabled
                if (gps.canGetLocation()) {
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    startZomatoTask(latitude, longitude);
                } else {
                    // Can't get location.
                    // GPS or Network is not enabled.
                    // Ask user to enable GPS/network in settings.
                    gps.showSettingsAlert();
                }
            } else {
                // Give default value if no location found.
                Log.e(CLASS_NAME, "No network connection. Using default longitude and latitude.");
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                startZomatoTask(latitude, longitude);
            }
        } catch (IOException ioe) {
            // Error.
            Log.e(CLASS_NAME, ioe.getMessage());
        }
    }

    /**
     * Starts a
     * @param latitude
     * @param longitude
     * @throws UnsupportedEncodingException
     * @throws MalformedURLException
     */
    private void startZomatoTask(double latitude, double longitude) throws UnsupportedEncodingException, MalformedURLException {
        URL urlString = new URL("https://developers.zomato.com/api/v2.1/geocode?lat=" + URLEncoder.encode(latitude + "", "UTF-8") + "&lon=" + URLEncoder.encode(longitude + "", "UTF-8"));

        ListView listView = (ListView) findViewById(R.id.restoList);

        ZomatoTask task = new ZomatoTask(this, listView,latitude,longitude);
        task.execute(urlString);


        Toast.makeText(this, "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
    }
}

package com.restobrownegg.near;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.restobrownegg.R;
import com.restobrownegg.tasks.HerokuTaskRestosReviews;
import com.restobrownegg.tasks.HerokuTaskSubmitReviews;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Shows all the reviews related to the current restaurant clicked.
 *
 * @author Shifat Khan
 */
public class ReviewActivity extends AppCompatActivity {
    private final String CLASS_NAME = "ReviewActivity";

    private int restoId = -1;

    private EditText titleInput;
    private EditText contentInput;
    private Spinner ratingSpinner;
    private String[] ratings;
    private Button submitBtn;

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);

        // Get the restaurant clicked and load the reviews related to it.
        if(getIntent().getIntExtra("restoid", -1) != -1){
            restoId = getIntent().getIntExtra("restoid", 0);
        }

        listView = (ListView) findViewById(R.id.reviewList);

        titleInput = (EditText) findViewById(R.id.titleInput);
        contentInput = (EditText) findViewById(R.id.contentInput);
        submitBtn = (Button) findViewById(R.id.submitButton);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitClick(view);
            }
        });

        ratingSpinner = (Spinner) findViewById(R.id.ratingSpinner);
        ratings = getResources().getStringArray(R.array.rating);

        // ArrayAdapter to populate the spinner for ratings.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,  Arrays.asList(ratings));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ratingSpinner.setAdapter(adapter);

        loadReviews();
    }

    /**
     * Loads the reviews related to the restaurant from heroku. It uses an Async task from
     * HerokuTaskRestosReviews that will then populate the listview.
     */
    private void loadReviews(){
        HerokuTaskRestosReviews hTask = new HerokuTaskRestosReviews(listView, this);
        Log.i(CLASS_NAME, "loadReviews for restoid: " + restoId);
        hTask.execute("https://young-shelf-72681.herokuapp.com/api/apiv1/getrestoreviews/", restoId+"");
    }

    /**
     * Submits the review to heroku.
     * @param v
     */
    private void submitClick(View v){
        Log.i(CLASS_NAME, "submitClick");
        String title = titleInput.getText().toString();
        String content = contentInput.getText().toString();
        int rating = Integer.parseInt(ratingSpinner.getSelectedItem().toString());

        if(validateFields(title,content,rating)){
            Log.i(CLASS_NAME, "Valid fields");
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected())
            {

                HerokuTaskSubmitReviews task = new HerokuTaskSubmitReviews();
                SharedPreferences prefs = getSharedPreferences("userInfo",MODE_PRIVATE);


                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("email",prefs.getString("email",""));
                    jsonObject.put("password", prefs.getString("password",""));
                    jsonObject.put("restoid", restoId);
                    jsonObject.put("title", title);
                    jsonObject.put("content", content);
                    jsonObject.put("rating", rating);
                }
                catch(JSONException jse)
                {
                    Log.e(CLASS_NAME,jse.getMessage());

                }

                task.execute("https://young-shelf-72681.herokuapp.com/api/apiv1/addreview", jsonObject.toString());
                alertMessages("Review submitted!");
                loadReviews();
            }
            else
                Log.e(CLASS_NAME, "Failed to sync to heroku");

            resetFields();
        }
    }

    private void resetFields(){
        titleInput.setText("");
        contentInput.setText("");
    }

    /**
     * Checks if the fields are valid or not. Displays an error message if not.
     * @param title
     * @param content
     * @param rating
     * @return Boolean indicating if valid.
     */
    private boolean validateFields(String title, String content, int rating){
        boolean isValid = true;

        // Error message to be displayed if there are any bad inputs.
        String errorMsg = "Invalid ";

        if(title == null || title.length()== 0) {
            Log.i(CLASS_NAME, "title: Invalid value");
            errorMsg += "Title, ";
            isValid = false;
        }
        if (content == null || content.length() == 0) {
            Log.i(CLASS_NAME, "content: Invalid value");
            errorMsg += "Content, ";
            isValid = false;
        }
        if(!(rating == 1 || rating == 2 || rating == 3 || rating == 4 || rating == 5)){
            Log.i(CLASS_NAME, "rating: Invalid value");
            errorMsg += "Rating. ";
            isValid = false;
        }

        // Display error message if there was an invalid field.
        if(!isValid)
            alertMessages(errorMsg);

        return isValid;
    }

    /**
     * Displays a prompt to the user in order to display a message. Usually used for warnings and/or
     * errors.
     * @param msg to display
     */
    private void alertMessages(String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg);
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}

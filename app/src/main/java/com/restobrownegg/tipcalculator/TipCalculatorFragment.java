package com.restobrownegg.tipcalculator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.restobrownegg.R;

/**
 * This class will be doing calculations that will result into a final tip result. It accepts the
 * total amount of payment needed, number of people and percentage of tip needed. It will then tell
 * the final amount of cash needed to pay the bill and how much each person need to pay.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class TipCalculatorFragment extends Fragment {

    private EditText billAmount;
    private EditText numOfPpl;

    private RadioButton tip10;
    private RadioButton tip15;
    private RadioButton tip20;
    private RadioGroup radioGroup;

    private TextView tipAmountDisplay;
    private TextView totalAmountDisplay;
    private TextView totalPersonDisplay;

    private Button btnCalc;
    private Button btnReset;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tip_calculator, container, false);

        // Get a handle on the widgets.
        billAmount = (EditText) v.findViewById(R.id.editBillAmount);
        numOfPpl = (EditText) v.findViewById(R.id.editNumOfPpl);

        radioGroup = (RadioGroup) v.findViewById(R.id.radioGroup);
        tip10 = (RadioButton) v.findViewById(R.id.tip10);
        tip15 = (RadioButton) v.findViewById(R.id.tip15);
        tip20 = (RadioButton) v.findViewById(R.id.tip20);
        // Default value
        radioGroup.check(R.id.tip15);

        tipAmountDisplay = (TextView) v.findViewById(R.id.displayTipAmount);
        totalAmountDisplay = (TextView) v.findViewById(R.id.displayTotalPayment);
        totalPersonDisplay = (TextView) v.findViewById(R.id.displayTotalPerPerson);

        btnCalc = (Button) v.findViewById(R.id.btnCalc);
        btnReset = (Button) v.findViewById(R.id.btnReset);

        // Set click listener that will calculate everything.
        btnCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCalc();
            }
        });

        // Set click listener that will reset all the fields.
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onReset();
            }
        });

        return v;
    }

    /**
     * Calculates everything by getting the bill amount, the number of people and the percentage of
     * tip needed to pay. It will then display the results on screen onto the view.
     */
    public void onCalc()
    {
        String billAmountValue = billAmount.getText().toString().trim();
        String numOfPplValue = numOfPpl.getText().toString().trim();

        // Validate the fields
        boolean isValid = validateFields(billAmountValue,numOfPplValue);

        // Display error message if not valid.
        if(!isValid)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Invalid data");
            builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        // If valid, we calculate.
        else
        {
            double billInt = Double.parseDouble(billAmountValue);
            int numPpl = Integer.parseInt(numOfPplValue);

            int tip;
            // Check the amount of tip.
            if(tip10.isChecked())
                tip = 10;
            else if(tip15.isChecked())
                tip = 15;
            else
                tip = 20;

            // Perform calculations.
            double tipAmount = billInt *tip /100;
            double total = billInt + tipAmount;
            double eachPerson = Math.round(total/numPpl * 100.0)/100.0;

            // Display results.
            tipAmountDisplay.setText(String.valueOf(tipAmount));
            totalAmountDisplay.setText(String.valueOf(total));
            totalPersonDisplay.setText(String.valueOf(eachPerson));


        }

    }

    /**
     * Reset all the edit texts into blanks fields.
     */
    public void onReset()
    {
        billAmount.setText("");
        numOfPpl.setText("");
        tipAmountDisplay.setText("");
        totalPersonDisplay.setText("");
        totalAmountDisplay.setText("");
    }

    /**
     * Validate the two fields for billAmountValue and numOfPplValue.
     * @param billAmountValue
     * @param numOfPplValue
     * @return
     */
    private boolean validateFields(String billAmountValue, String numOfPplValue)
    {
        boolean isValid = true;
        try {
            Double.parseDouble(billAmountValue);
            Integer.parseInt(numOfPplValue);

        }

        catch(NumberFormatException nfe)
        {
            isValid=false;
        }

        return isValid;
    }
}

package com.restobrownegg.listadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.restobrownegg.R;
import com.restobrownegg.bean.Review;

import java.util.ArrayList;

/**
 * This class will be taking care of handling an arrayAdapter in order to display the required info
 * about a certain review. It takes in a list of reviews that will then be used to populate a listview
 * with all the informations related to that review.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class ReviewListAdapter extends ArrayAdapter<Review>{

    private final String CLASS_NAME = "RestoListAdapter";

    private ArrayList<Review> reviewsList = new ArrayList<>();
    private Context context;

    private static LayoutInflater inflater = null;

    /**
     * Two parameter constructor that takes in the context of which this adapter will find itself
     * in, and a list of reviews that will be used to populate the list view.
     *
     * @param context Context
     * @param reviewsList ArrayList<Review>
     */
    public ReviewListAdapter(Context context, ArrayList<Review> reviewsList){
        super(context, 0, reviewsList);

        this.context = context;
        this.reviewsList = reviewsList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        // Inflate the view to populate.
        if (convertView == null) {
            v = inflater.inflate(R.layout.review_name_fields, null);
        }

        // Set info onto the view.
        TextView reviewTitleTv = (TextView) v.findViewById(R.id.reviewTitleLbl);
        TextView reviewContentTv = (TextView) v.findViewById(R.id.reviewContentLbl);

        Log.i(CLASS_NAME,"Display review at position: " + position);

        // Display info about review onto the view.
        reviewTitleTv.setText(reviewsList.get(position).getTitle());
        reviewContentTv.setText(reviewsList.get(position).getContent());

        // Get a handle on the star images (used to visually display the ratings).
        ImageView star1 = (ImageView) v.findViewById(R.id.star1);
        ImageView star2 = (ImageView) v.findViewById(R.id.star2);
        ImageView star3 = (ImageView) v.findViewById(R.id.star3);
        ImageView star4 = (ImageView) v.findViewById(R.id.star4);
        ImageView star5 = (ImageView) v.findViewById(R.id.star5);

        // Convert the ratings into a visual representation with the image views.
        setStars(reviewsList.get(position).getRating(), star1, star2, star3, star4, star5);

        return v;
    }

    /**
     * Checks the amount of starts the user put in the rating int and then displays that many amount
     * of stars onto the view. The stars are all image views that are invisible that will then be
     * visible based on the amount of ratings.
     *
     * @param rating int
     * @param star1 ImageView
     * @param star2 ImageView
     * @param star3 ImageView
     * @param star4 ImageView
     * @param star5 ImageView
     */
    private void setStars(int rating, ImageView star1, ImageView star2, ImageView star3, ImageView star4, ImageView star5){
        switch (rating){
            case 1:
                star1.setVisibility(View.VISIBLE);
                break;
            case 2:
                star1.setVisibility(View.VISIBLE);
                star2.setVisibility(View.VISIBLE);
                break;
            case 3:
                star1.setVisibility(View.VISIBLE);
                star2.setVisibility(View.VISIBLE);
                star3.setVisibility(View.VISIBLE);
                break;
            case 4:
                star1.setVisibility(View.VISIBLE);
                star2.setVisibility(View.VISIBLE);
                star3.setVisibility(View.VISIBLE);
                star4.setVisibility(View.VISIBLE);
                break;
            case 5:
                star1.setVisibility(View.VISIBLE);
                star2.setVisibility(View.VISIBLE);
                star3.setVisibility(View.VISIBLE);
                star4.setVisibility(View.VISIBLE);
                star5.setVisibility(View.VISIBLE);
                break;
        }
    }
}

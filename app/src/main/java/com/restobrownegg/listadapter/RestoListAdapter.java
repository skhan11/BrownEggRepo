package com.restobrownegg.listadapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.restobrownegg.R;
import com.restobrownegg.bean.Restaurant;
import com.restobrownegg.details.ShowActivity;
import com.restobrownegg.details.ShowFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * This class extends the ArrayAdapter since it will be used for displaying data related to a
 * restaurant on a listview. It will also launch a fragment that will display all information
 * related to the resto in question when it's clicked.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class RestoListAdapter extends ArrayAdapter<String> {
    private final String CLASS_NAME = "RestoListAdapter";

    private ArrayList<String> restoNames = new ArrayList<>();
    private ArrayList<Restaurant> restoItems = new ArrayList<>();

    private Context context;

    private static LayoutInflater inflater = null;

    private View detailsFrame;

    private FragmentManager fm;


    /**
     * 3 parameters constructor that takes in the context of which this adapter will find itself in,
     * the list of resto names and the list of restaurant objects in order to later display them.
     * @param context Context
     * @param restoNames ArrayList<String>
     * @param restoItems ArrayList<Restaurant>
     */
    public RestoListAdapter(Context context, ArrayList<String> restoNames, ArrayList<Restaurant> restoItems) {
        super(context, 0, restoNames);
        this.restoNames = restoNames;
        this.restoItems = restoItems;
        this.context = context;
        fm = ((Activity) context).getFragmentManager();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        // Inflate the view that will be used to display the resto's info.
        if (convertView == null) {
            v = inflater.inflate(R.layout.resto_name_fields, null);
        }

        // Check whether we can display the FrameLayout or not.
        detailsFrame = parent.findViewById(R.id.restoDetailsFrameLayout);

        TextView tv = (TextView) v.findViewById(R.id.restoNames);
        tv.setText(restoNames.get(position));

        final TextView phoneTv = (TextView) v.findViewById(R.id.restoPhone);
        phoneTv.setText(restoItems.get(position).getTelephoneNumber());

        // Set a click listener that will launch the ShowFragment in order to display data related
        // to the restaurant clicked.
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Vars used to check the screen orientation.
                Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
                int rotation = display.getRotation();

                // Check if we can display the restaurant details as a fragment or not (for landscape).
                if (rotation == Surface.ROTATION_90
                        || rotation == Surface.ROTATION_270) {
                    Log.i(CLASS_NAME, "Landscape: ShowFragment");
                    ShowFragment details = new ShowFragment();

                    // Put all informations about the restaurant clicked in a bundle and send it to
                    // the intent.
                    Bundle args = new Bundle();
                    args.putString("restoName", restoItems.get(position).getRestoName());
                    args.putString("restoGenre", restoItems.get(position).getRestoGenre());
                    args.putInt("restoPriceRange", restoItems.get(position).getRestoPriceRange());
                    args.putString("address", restoItems.get(position).getAddress());
                    args.putString("city", restoItems.get(position).getCity());
                    args.putString("postalCode", restoItems.get(position).getPostalCode());
                    args.putDouble("latitude", restoItems.get(position).getLatitude());
                    args.putDouble("longitude", restoItems.get(position).getLongitude());
                    args.putString("telephoneNum", restoItems.get(position).getTelephoneNumber());
                    args.putString("source", restoItems.get(position).getSource());

                    // Convert the dates into a String.
                    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                    String created = df.format(restoItems.get(position).getCreatedDate());
                    String updated = df.format(restoItems.get(position).getUpdatedDate());

                    args.putString("createdDate", created);
                    args.putString("updatedDate", updated);

                    details.setArguments(args);
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.restoDetailsFrameLayout, details);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.addToBackStack(null).commit();
                }
                else {
                    // Portrait mode
                    Log.i(CLASS_NAME, "Portrait: ShowActivity");
                    Intent intent = new Intent(context, ShowActivity.class);

                    // Put all informations about the restaurant clicked in a bundle and send it to
                    // the intent.
                    intent.putExtra("restoName", restoItems.get(position).getRestoName());
                    intent.putExtra("restoGenre", restoItems.get(position).getRestoGenre());
                    intent.putExtra("restoPriceRange", restoItems.get(position).getRestoPriceRange());
                    intent.putExtra("address", restoItems.get(position).getAddress());
                    intent.putExtra("city", restoItems.get(position).getCity());
                    intent.putExtra("postalCode", restoItems.get(position).getPostalCode());
                    intent.putExtra("latitude", restoItems.get(position).getLatitude());
                    intent.putExtra("longitude", restoItems.get(position).getLongitude());
                    intent.putExtra("telephoneNum", restoItems.get(position).getTelephoneNumber());
                    intent.putExtra("source", restoItems.get(position).getSource());

                    // If resto is from heroku, we send the restoId as well in order to load its reviews.
                    if(restoItems.get(position).getSource().equals("heroku"))
                        intent.putExtra("restoid",restoItems.get(position).getRestoId());

                    // Convert the dates into Strings.
                    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                    String created = df.format(restoItems.get(position).getCreatedDate());
                    String updated = df.format(restoItems.get(position).getUpdatedDate());

                    intent.putExtra("createdDate", created);
                    intent.putExtra("updatedDate", updated);

                    context.startActivity(intent);
                }
            }
        });

        // Set a long click listener that will launch the phone dial for the restaurant's phone number.
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                try{
                    // Start an implicit intent for dial (doesn't call directly, lets user double check.)
                    Intent intent = new Intent(Intent.ACTION_DIAL);

                    Log.i(CLASS_NAME, "Calling: " + phoneTv.getText().toString());

                    intent.setData(Uri.parse("tel:" + phoneTv.getText().toString()));
                    context.startActivity(intent);
                }catch (ActivityNotFoundException e){
                    Log.e(CLASS_NAME, e.getMessage());
                }

                return true;
            }
        });

        return v;
    }
}

package com.restobrownegg.favourite;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.database.Cursor;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.restobrownegg.home.MenuActivity;
import com.restobrownegg.R;
import com.restobrownegg.dao.DBHelper;

/**
 * This class will display all the restaurants in a List View. When the user clicks on one of the
 * restaurants, it will add that restaurant to the user's favorite table.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class FavouriteSelectRestosActivity extends MenuActivity {
    private final String CLASS_NAME = "SelectFavouriteActivity";

    private static DBHelper dbh;

    private SimpleCursorAdapter sca;
    private Cursor cursor;

    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_resto_for_favourites);

        Log.i(CLASS_NAME, "onCreate()");

        // Define what we want to retrieve from the database.
        String [] from = {DBHelper.COL_RESTO_NAME, DBHelper.COL_GENRE, DBHelper.COL_ID, DBHelper.COL_PHONE_NUMBER};

        // Define where the columns will be displayed.
        int [] to = {R.id.restoName, R.id.restoGenre, R.id.restoId, R.id.restoTelephoneNum};

        // Get the email of the current user.
        SharedPreferences prefs = getSharedPreferences("userInfo", MODE_PRIVATE);
        String email = prefs.getString("email","");

        dbh = DBHelper.getDBHelper(this);


        ListView lv = (ListView) findViewById(R.id.addToFavList);

        //checking if the user is logged in
        if(email.length() != 0){
            //get all the resto info from the database
            cursor = dbh.getAllRestoInfo();
            sca = new SimpleCursorAdapter(this, R.layout.resto_list, cursor, from, to, 0);


            Log.i(CLASS_NAME, "Restos found: "+cursor.getCount());

            //takes care of displaying all the restos from the database and it puts an addToFav
            //onClickListener
            lv.setAdapter(sca);
            lv.setOnItemClickListener(addToFavourite);
        }

        Log.i(CLASS_NAME,"finished onCreate()");
    }

    /**
     * This method takes care of adding the restaurants to the favourites table.  It will get
     * the resto id and pass it to the private addToFavourite method in order to add the resto
     * to the favourites table.
     */
    public OnItemClickListener addToFavourite = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.i(CLASS_NAME, "onItemClick() for position: " + position);

            // Get the item view that is selected in order to retrieve the resto's id.
            TextView restoIdTextView = (TextView) view.findViewById(R.id.restoId);
            int restoId = Integer.parseInt(restoIdTextView.getText().toString());

            addToFavourite(restoId);

            //used for output
            TextView restoNameTextView = (TextView) view.findViewById(R.id.restoName);
            String restoName = restoNameTextView.getText().toString();

            alertMessages(restoName+" added to favourites");
        }
    };

    /**
     * Adds a particular restaurant to the current user's favourite table.
     * Gets the email from the shared preferences and then sends both restoId and email as params
     * to an insert method in the DAO.
     *
     * @param restoId
     */
    private void addToFavourite(int restoId){
        Log.i(CLASS_NAME, "addToFavourite() for restoId: " + restoId);
        SharedPreferences prefs = getSharedPreferences("userInfo", MODE_PRIVATE);
        String email = prefs.getString("email","");

        dbh.addFavourite(restoId, email);
    }

    /**
     * Displays a prompt to the user in order to display a message. Usually used for warnings and/or
     * errors.
     * @param msg to display
     */
    private void alertMessages(String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg);
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}

package com.restobrownegg.favourite;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.restobrownegg.R;
import com.restobrownegg.details.RestoDetailsActivity;
import com.restobrownegg.details.RestoDetailsFragment;
import com.restobrownegg.dao.DBHelper;

/**
 * This class takes care of displaying the favourite restos of a user by searching through the database
 * based on the user's id (email for this case). The user can add additional restaurants to its
 * favourites database by clicking the add button.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class FavouriteRestosActivity extends AppCompatActivity {

    private final String CLASS_NAME = "FavouriteRestosActivity";

    private static DBHelper dbh;

    private SimpleCursorAdapter sca;
    private Cursor cursor;

    private int selection = 0;

    private boolean mDualPane = false;

    Button addRestoBtn;

    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_restos);

        Log.i(CLASS_NAME, "onCreate()");

        //launches the openRestoList which shows a list of restos that you can add
        addRestoBtn = (Button)findViewById(R.id.addRestoBtn);
        addRestoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the FavouriteSelectRestosActivity.
                openRestoList();
            }
        });

        // Restore state if a resto was previously selected.
        if(savedInstanceState != null && savedInstanceState.getInt("selection", 0) != 0){
            selection = savedInstanceState.getInt("selection", 0);
        }

        Log.i(CLASS_NAME, "finished onCreate()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(CLASS_NAME,"onResume()");
        // Define what we want to retrieve from the database.
        String [] from = {DBHelper.COL_RESTO_NAME, DBHelper.COL_GENRE, DBHelper.COL_ID, DBHelper.COL_PHONE_NUMBER};

        // Define where the columns will be displayed.
        int [] to = {R.id.restoName, R.id.restoGenre, R.id.restoId, R.id.restoTelephoneNum};

        //Getting the shared prefs to get the email of the user
        SharedPreferences prefs = getSharedPreferences("userInfo",MODE_PRIVATE);
        String email = prefs.getString("email","");

        // Get a handle on the listview showing all the favourite restos.
        ListView lv = (ListView) findViewById(R.id.restoFoundList);

        // Display the favourites only if user is logged in.
        if(email.length() != 0){
            dbh = DBHelper.getDBHelper(this);

            cursor = dbh.getFavouriteRestos(email);
            sca = new SimpleCursorAdapter(this, R.layout.resto_list, cursor, from, to, 0);

            Log.i(CLASS_NAME, "Favourites found: " + cursor.getCount());

            //This will set an onItemLongClickListener in order to call the restaurant from the list.
            lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            lv.setOnItemClickListener(onClick);
            lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
                {
                    try{
                        // Start an implicit intent for dial
                        Intent intent = new Intent(Intent.ACTION_DIAL);

                        TextView numtxt = (TextView) arg1.findViewById(R.id.restoTelephoneNum);

                        Log.i(CLASS_NAME, "Calling: " + numtxt.getText().toString());

                        intent.setData(Uri.parse("tel:" + numtxt.getText().toString()));
                        context.startActivity(intent);
                    }catch (ActivityNotFoundException e){
                        Log.e(CLASS_NAME, e.getMessage());
                    }

                    return true;
                }

            });

            lv.setAdapter(sca);

            View detailsFrame = findViewById(R.id.restoDetailsFrameLayout);

            mDualPane = detailsFrame != null
                    && detailsFrame.getVisibility() == View.VISIBLE;
            //Checks if you can show fragment in the frame layout
            //if its portrait then you can't show the fragment in the frame layout
            //if mDualPane is true then it is landscape else it is portrait
            if(mDualPane && cursor != null && cursor.getCount() != 0){
                // Simulate an onclick for the item.
                // As shown by Ed. S from stackoverflow (first answer from the forum):
                // - http://stackoverflow.com/questions/8611612/how-to-trigger-onlistitemclick-in-a-listfragment
                lv.requestFocusFromTouch();
                lv.setSelection(selection);
                lv.performItemClick(lv.getAdapter().getView(selection, null, null), selection, selection);
            }
            else{
                lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                lv.setItemChecked(selection, true);
            }
        }
        Log.i(CLASS_NAME,"finished onResume()");
    }

    /**
     * On item click listener that will get the restoId and open the details for that resto.
     *
     */
    private AdapterView.OnItemClickListener onClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView restoNameTextView = (TextView) view.findViewById(R.id.restoName);
            //used for logging
            String restoName = restoNameTextView.getText().toString();
            Log.i(CLASS_NAME, "Resto clicked: " + restoName);

            // Get the item view that is selected in order to retrieve the resto's id.
            TextView restoIdTextView = (TextView) view.findViewById(R.id.restoId);
            int restoId = Integer.parseInt(restoIdTextView.getText().toString());

            showRestoDetails(position, restoId);
        }
    };

    /**
     * Opens the RestoDetailsFragment to show all details related to a restaurant. It checks if
     * it can display the fragment in the FrameLayout or not.
     *
     * @param index - currently selected resto.
     * @param restoId - the restoId of the resto clicked.
     */
    private void showRestoDetails(int index, int restoId){
        Log.i(CLASS_NAME, "showRestoDetails()");
        selection = index;
        //if true then it is landscape
        if(mDualPane){
            Log.i(CLASS_NAME, "landscape");
            //get the frame layout that will be displayed to the right of the tablet
            RestoDetailsFragment details = (RestoDetailsFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.restoDetailsFrameLayout);

            if(details == null || details.getShownIndex() != index){
                // Show new restaurant selection
                details = RestoDetailsFragment.newInstance(index);

                // Pass in the restoId so we can later search through the database.
                Bundle args = new Bundle();
                args.putInt("restoId", restoId);
                args.putInt("index", index);
                details.setArguments(args);

                 /* takes care of replacing the layout on the right when it is in landscape
                  * fragment simply fades out
                  * by making add to backstack null it will close the fragment on the frame layout
                  * on the right rather than closing the app
                  * */
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.restoDetailsFrameLayout, details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null).commit();
            }// End inner if.
        }// End outer if.
        //false therefore portrait
        else {
            Log.i(CLASS_NAME, "portrait");
            // Start the RestoDetailsActivity since we know the screen is in portrait mode.
            Intent intent = new Intent();

            intent.setClass(this, RestoDetailsActivity.class);

            // Put in the current index.
            intent.putExtra("index", index);
            intent.putExtra("restoId", restoId);

            // Start the RestoDetailsActivity activity.
            startActivity(intent);
        }// End if - else.


    }

    /**
     * Opens a new activity containing a list of all the restaurants from the database. Used to
     * let the user select a resto that will be then added to its favourites list.
     */
    private void openRestoList(){
        Log.i(CLASS_NAME,"openRestoList()");
        // Start the FavouriteSelectRestosActivity.
        Intent intent = new Intent();

        intent.setClass(this, FavouriteSelectRestosActivity.class);

        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.i(CLASS_NAME,"saving selection");
        // Save the current selection to restore state.
        outState.putInt("selection", selection);
    }
}

package com.restobrownegg.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.restobrownegg.bean.Review;
import com.restobrownegg.listadapter.ReviewListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


import javax.net.ssl.HttpsURLConnection;


/**
 * This class will be taking in reviews from Heroku and process the data in order to later
 * display it onto the views.  The list and context are passed because the adapter is called
 * in the postexecute and that adapter needs these two values to display the reviews.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class HerokuTaskRestosReviews extends AsyncTask<String, Void, ArrayList<Review>> {

    private final String CLASS_NAME = "HerokuTaskRestosReviews";

    private Context context;
    private ArrayList<Review> reviewList;
    private ListView listView;

    /**
     * Context and listview is used by the adapter to display
     * @param listView
     * @param context
     */
    public HerokuTaskRestosReviews(ListView listView, Context context) {
        this.context = context;
        this.listView = listView;
    }


    protected ArrayList<Review> doInBackground(String... params) {
        Log.i(CLASS_NAME, "doInBackground");

        InputStream is = null;
        int response;
        URL url = null;
        StringBuilder stringBuilder = new StringBuilder();
        HttpsURLConnection urlConnection = null;
        BufferedReader reader = null;
        //takes care of forming the url in order to perform the get operation
        try {
            url = new URL(params[0] + params[1]);
        } catch (MalformedURLException exception) {
            Log.e(CLASS_NAME, exception.getMessage());
        }

        try {

            urlConnection = (HttpsURLConnection) url.openConnection();

            urlConnection.setRequestMethod("GET");

            urlConnection.setReadTimeout(10000);
            //milliseconds
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

            //receives the response if its not 200 then something went wrong
            int responseCode = urlConnection.getResponseCode();
            Log.i(CLASS_NAME, responseCode + "");
            if (responseCode == HttpURLConnection.HTTP_OK) {
                //adding the json data to a string builder in order to parse it in json objects
                //this whole section reads from a buffer and adds it to a string builder
                is = urlConnection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(is));
                String line;


                while ((line = reader.readLine()) != null)
                    stringBuilder.append(line);
                is.close();
                reader.close();
                Log.i(CLASS_NAME, "JSONArray: "+stringBuilder.toString());

                //parsing the string builder to a json array, that is how the php side set it
                JSONArray userReviews = new JSONArray(stringBuilder.toString());

                int numOfReviews = userReviews.length();
                reviewList = new ArrayList<>();

                /** this loop will go through all the reviews received from the json data
                * and set the data of each fields in a reviews bean. This review bean will
                * be added to a review list. All of the fields received from heroku match
                * ones in the bean and the database.
                * */
                for (int i = 0; i < numOfReviews; i++) {
                    Review reviewObject = new Review();
                    JSONObject review = userReviews.getJSONObject(i);
                    Log.i(CLASS_NAME, "Got resto " + i + ", " + review.getString("title"));
                    reviewObject.setTitle(review.getString("title"));
                    reviewObject.setEmail(review.getString("email"));
                    reviewObject.setContent(review.getString("content"));
                    reviewObject.setRestoId(params[1]);
                    reviewObject.setRating(review.getInt("rating"));

                    reviewList.add(reviewObject);
                }


            }
        } catch (Exception e) {
            Log.e(CLASS_NAME, e.getMessage());
        }

        Log.i(CLASS_NAME, "reviewList count: " + reviewList.size());

        return reviewList;
    }

    protected void onPostExecute(ArrayList<Review> reviews) {
        Log.i(CLASS_NAME, "onPostExecute()");

        //passes necessary data to display the reviews
        ReviewListAdapter listAdapter = new ReviewListAdapter(context, reviewList);

        listView.setAdapter(listAdapter);
    }
}
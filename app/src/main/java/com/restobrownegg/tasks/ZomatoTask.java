package com.restobrownegg.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.restobrownegg.bean.Restaurant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * This class is an AsyncTask that will be fetching nearby restaurants by the help of an asynctask
 * and an inner HerokuTaskRestosNearby. It will send the json object containing the appropriate info
 * in order to send it to zomato as a GET request.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */

public class ZomatoTask extends AsyncTask<URL, Void, ArrayList<Restaurant>> {

    private Context context;
    private ListView listView;
    private final String CLASS_NAME = "ZomatoTask";
    private double longitude;
    private double latitude;

    /**
     * Four parameter constructor taking in the context of which this task will be ran, the listview
     * of which we want to populate with nearby restos, the latitude and longitude of our position.
     * @param context Context
     * @param listView ListView
     * @param latitude double
     * @param longitude double
     */
    public ZomatoTask(Context context, ListView listView, double latitude, double longitude)
    {
        this.context = context;
        this.listView = listView;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    protected ArrayList<Restaurant> doInBackground(URL... url) {
        Log.i(CLASS_NAME, "doInBackground");

        // Vars to send request.
        StringBuilder sb = new StringBuilder();
        ArrayList<Restaurant> list = new ArrayList<>();
        InputStream is = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            // Prepare connection and connect to the url.
            urlConnection = (HttpURLConnection) url[0].openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("user-key", "44761b717a2b43da8e70e208dc628db0");
            urlConnection.connect();

            // Check response code to see if it was successful
            int response = urlConnection.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                is = urlConnection.getInputStream();

                // Set a buffer.
                reader = new BufferedReader(new InputStreamReader(is));
                String line;

                while ((line = reader.readLine()) != null)
                    sb.append(line);
                is.close();
                reader.close();

                // JSONObject to hold incoming restos.
                JSONObject jsonData = new JSONObject(sb.toString());

                JSONArray nearbyRestaurants = jsonData.getJSONArray("nearby_restaurants");


                // Loop through each json objects in order to fetch each resto's info.
                for (int i = 0; i < nearbyRestaurants.length(); i++) {
                    Restaurant restaurant = new Restaurant();
                    JSONObject restoCtr = nearbyRestaurants.getJSONObject(i);

                    JSONObject restoInfo = restoCtr.getJSONObject("restaurant");

                    restaurant.setRestoName(restoInfo.getString("name"));
                    restaurant.setRestoGenre(restoInfo.getString("cuisines"));
                    restaurant.setRestoPriceRange(restoInfo.getInt("price_range"));

                    //getting location
                    JSONObject restoLocation = restoInfo.getJSONObject("location");
                    restaurant.setAddress(restoLocation.getString("address"));
                    restaurant.setCity(restoLocation.getString("city"));
                    restaurant.setLatitude(Double.parseDouble(restoLocation.getString("latitude")));
                    restaurant.setLongitude(Double.parseDouble(restoLocation.getString("longitude")));
                    restaurant.setPostalCode(restoLocation.getString("zipcode"));

                    restaurant.setSource("zomato");
                    //default to dawson because zomato does not have tel numbers
                    restaurant.setTelephoneNumber("5149318731");

                    Calendar c = Calendar.getInstance();
                    restaurant.setCreatedDate(c.getTime());
                    restaurant.setUpdatedDate(c.getTime());

                    // List of restaurants to return.
                    list.add(restaurant);

                    Log.i(CLASS_NAME, "Added " + i + "");

                }

                // Disconnect when done
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e(CLASS_NAME, e.getMessage());
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioe) {
                    //ignore
                }

            }
            if (reader != null) {
                try {
                    reader.close();

                } catch (IOException ioe) {
                    //ignore
                }
            }

            if (urlConnection != null) {
                try {
                    urlConnection.disconnect();
                } catch (IllegalStateException ise) {
                    //ignore
                }
            }
        }

        Log.i(CLASS_NAME, "StringBuilder: "+sb.toString());
        return list;
    }

    @Override
    protected void onPostExecute(ArrayList<Restaurant> restosFromZomatoList) {

        // Run an inner task to get the restos from heroku by supplying the longitude and latitude.
        URL url = null;
        try {
            url = new URL("https://young-shelf-72681.herokuapp.com/api/apiv1/getrestosnear/"+latitude +","+longitude);
        }

        catch(MalformedURLException exception)
        {
            Log.e(CLASS_NAME,exception.getMessage());
        }

        // Run the task.
        HerokuTaskRestosNearby hTask = new HerokuTaskRestosNearby(restosFromZomatoList,listView, context);
        hTask.execute(url);


    }
}

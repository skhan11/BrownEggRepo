package com.restobrownegg.tasks;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 *
 * This class will be adding reviews to Heroku. The data that will be passed is content,
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class HerokuTaskSubmitReviews extends AsyncTask<String, Void, String> {
    private final String CLASS_NAME = "HerokuTaskSubmitReviews";

    @Override
    protected String doInBackground(String... params) {
        InputStream is = null;
        OutputStream out;
        URL url;
        StringBuilder stringBuilder = new StringBuilder();
        HttpsURLConnection urlConnection = null;
        byte[] bytes;

        //takes care of parsing the json data to bytes because we are writing to a buffer
        try {
            bytes = params[1].getBytes("UTF-8");
        }
        catch(UnsupportedEncodingException exception)
        {
            Log.d(CLASS_NAME,exception.getMessage());
            return "UTF-8 is not supported";
        }

        Integer bytesLeng = bytes.length;
        //takes care of forming the url that will be used to send the post request
        try {
            url = new URL(params[0]);
        }
        catch(MalformedURLException exception)
        {
            Log.d(CLASS_NAME,exception.getMessage());
            return "URL is wrong";
        }

        try {
            //this block of code is necessary to start the post request operation with heroku
            urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod("POST");

            urlConnection.setReadTimeout(10000);
            //milliseconds
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            urlConnection.addRequestProperty("Content-Length", bytesLeng.toString());

            //here we are writing to a buffer, what we are writing is json data object
            out = new BufferedOutputStream(urlConnection.getOutputStream());

            out.write(bytes);
            out.flush();
            out.close();

            //
            int responseCode = urlConnection.getResponseCode();
            Log.i(CLASS_NAME,"Response code: "+responseCode);
            if(responseCode == HttpURLConnection.HTTP_OK) {
                is = urlConnection.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;

                while ((line = reader.readLine()) != null)
                    stringBuilder.append(line);
                is.close();
                Log.i(CLASS_NAME, stringBuilder.toString());

                JSONObject jsonData = new JSONObject(stringBuilder.toString());
            }

            urlConnection.disconnect();

        } catch (Exception e) {
            Log.e(CLASS_NAME, "Failed to receive data from heroku " + e.getMessage());
            e.printStackTrace();
        }

        return "";
    }
}

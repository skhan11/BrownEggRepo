package com.restobrownegg.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.restobrownegg.bean.Restaurant;
import com.restobrownegg.listadapter.RestoListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * This class takes care of getting the restos nearby from heroku. It takes a context, a zomatoArrayList and
 * a listView. The context and listview are used by the adapter to display on the device. The
 * zomatoArrayList is used to get the data to display. This class will merge the zomatoArrayList
 * with the heroku api in order to display all the restos nearby received from both apis.
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */

public class HerokuTaskRestosNearby extends AsyncTask<URL, Void, ArrayList<Restaurant>> {

    private final String CLASS_NAME = "HerokuTaskRestosNearby";
    private ArrayList<Restaurant> restosFromZomatoList;
    private Context context;
    private ListView listView;

    /**
     *
     * Context and ListView are used by the adapter to display the content. The restosFromZomatoList
     * is used to merge both zomato and heroku.
     * @param restosFromZomatoList ArrayList<Restaurant>
     * @param listView ListView
     * @param context Context
     */
    public HerokuTaskRestosNearby(ArrayList<Restaurant> restosFromZomatoList,ListView listView, Context context)
    {
        this.restosFromZomatoList = restosFromZomatoList;
        this.listView = listView;
        this.context = context;
    }


    protected ArrayList<Restaurant> doInBackground(URL... params) {
        Log.i(CLASS_NAME, "doInBackground");

        InputStream is = null;
        int response;
        URL url = null;
        StringBuilder stringBuilder = new StringBuilder();
        HttpsURLConnection urlConnection = null;
        ArrayList<Restaurant> restosFromApiList = new ArrayList<>();
        BufferedReader reader = null;


        //entering url that matches the php
        url = params[0];

        try {
            //starting a get connection which will return json data

            urlConnection = (HttpsURLConnection) url.openConnection();

            //GET
            urlConnection.setRequestMethod("GET");


            urlConnection.setReadTimeout(10000);
            //milliseconds
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");


            //the response that the php side gives if it is not 200 then something went wrong
            int responseCode = urlConnection.getResponseCode();
            Log.i(CLASS_NAME, responseCode + "");
            if (responseCode == HttpURLConnection.HTTP_OK) {
                //adding the json data to a string builder in order to parse it in json objects
                //this whole section reads from a buffer and adds it to a string builder
                is = urlConnection.getInputStream();


                reader = new BufferedReader(new InputStreamReader(is));
                String line;


                while ((line = reader.readLine()) != null)
                    stringBuilder.append(line);
                is.close();
                reader.close();
                Log.i(CLASS_NAME, stringBuilder.toString());

                //parsing the string builder to a json array, that is how the php side set it
                JSONArray nearbyRestos = new JSONArray(stringBuilder.toString());
                Log.i(CLASS_NAME, nearbyRestos.length() + "");

                //Need this because php side is formatting their dates differently
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH");

                /** this loop will go through all the restos nearby received from the json data
                 * and set the data of each fields in a restaurant bean. This restaurant bean will
                 * be added to a Restaurant list. All of the fields received from heroku match
                 * ones in the bean and the database.
                 * */
                for (int i = 0; i < nearbyRestos.length(); i++) {
                    Restaurant restaurant = new Restaurant();
                    JSONObject restosNearbyFromApi = nearbyRestos.getJSONObject(i);

                    restaurant.setRestoName(restosNearbyFromApi.getString("name"));
                    restaurant.setRestoGenre(restosNearbyFromApi.getString("genre"));
                    restaurant.setRestoPriceRange(restosNearbyFromApi.getInt("pricing"));
                    restaurant.setAddress(restosNearbyFromApi.getString("address"));
                    restaurant.setCity(restosNearbyFromApi.getString("city"));
                    restaurant.setPostalCode(restosNearbyFromApi.getString("postalcode"));
                    restaurant.setLatitude(restosNearbyFromApi.getDouble("latitude"));
                    restaurant.setLongitude(restosNearbyFromApi.getDouble("longitude"));

                    String createdDate = restosNearbyFromApi.getString("created_at");

                    restaurant.setCreatedDate(dateFormat.parse(createdDate));

                    String updatedDate = restosNearbyFromApi.getString("updated_at");

                    restaurant.setUpdatedDate(dateFormat.parse(updatedDate));

                    //default to dawson
                    restaurant.setTelephoneNumber("5149318731");

                    restaurant.setRestoId(restosNearbyFromApi.getInt("restoid"));

                    restaurant.setSource("heroku");

                    restosFromApiList.add(restaurant);

                    Log.i(CLASS_NAME, "Added: " + i + " " + restaurant.getRestoName());
                }

            }

            //need to close connection and finally block deals with closing resources
            urlConnection.disconnect();

        } catch (Exception e) {
            Log.i(CLASS_NAME, e.getMessage());

        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioe) {
                    //ignore
                }

            }
            if (reader != null) {
                try {
                    reader.close();

                } catch (IOException ioe) {
                    //ignore
                }
            }

            if (urlConnection != null) {
                try {
                    urlConnection.disconnect();
                } catch (IllegalStateException ise) {
                    //ignore
                }
            }

            return restosFromApiList;
        }
    }

    protected void onPostExecute(ArrayList<Restaurant> restosFromApiList) {
        ArrayList<Restaurant> restoItems  = new ArrayList<>();

        //This part takes care of inserting all names of resto received from both apis onto a list
        //that will be used by the list adapter, it also inserts restaurant objects received from
        //both list into a single list
        ArrayList<String> restoNames = new ArrayList<>();
        for (int i = 0; i < restosFromZomatoList.size(); i++) {
            restoNames.add(restosFromZomatoList.get(i).getRestoName());
            restoItems.add(restosFromZomatoList.get(i));

        }
        for(int i = 0; i < restosFromApiList.size(); i++) {
            restoNames.add(restosFromApiList.get(i).getRestoName());
            restoItems.add(restosFromApiList.get(i));
        }



        //displays the list of restos
        RestoListAdapter listAdapter = new RestoListAdapter(context, restoNames, restoItems);

        listView.setAdapter(listAdapter);

    }
}

package com.restobrownegg.tasks;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * This is an AsyncTask that will take care of writing to the heroku database by sending a POST request
 * that will containt a bytes array (json) of what we want to write.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class SyncTask extends AsyncTask<String, Void, Void> {

    private ArrayList<String> jsonObjectArrayList;

    public SyncTask(ArrayList<String> jsonObjectArrayList) {
        this.jsonObjectArrayList = jsonObjectArrayList;
    }

    private final String CLASS_NAME = "SyncTask";

    @Override
    protected Void doInBackground(String... params) {
        Log.i(CLASS_NAME, "doInBackground()");

        // Vars used to write to heroku.
        OutputStream out = null;
        int response;
        URL url = null;
        HttpsURLConnection urlConnection = null;
        byte[] bytes = null;
        Integer bytesLength = null;

        // Get the url of the heroku.
        try {
            url = new URL(params[0]);
        } catch (MalformedURLException exception) {
            Log.d(CLASS_NAME, exception.getMessage());

        }

        Log.i(CLASS_NAME, "JsonObject arraylist size: " + jsonObjectArrayList.size());

        // Loop through the jsonObject list and send it to the heroku server as POST request.
        for (int i = 0; i < jsonObjectArrayList.size(); i++) {
            try {

                // Get the bytes array of the json object.
                bytes = jsonObjectArrayList.get(i).getBytes();
                Log.i(CLASS_NAME, "json object #"+i);
                bytesLength = bytes.length;


                // Prepare the connection to heroku.
                urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setRequestMethod("POST");

                urlConnection.setReadTimeout(10000);
                //milliseconds
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                urlConnection.addRequestProperty("Content-Length", bytesLength.toString());

                // Create a Buffer for the thread.
                out = new BufferedOutputStream(urlConnection.getOutputStream());

                out.write(bytes);
                out.flush();
                out.close();


                int responseCode = urlConnection.getResponseCode();
                Log.i(CLASS_NAME, "Response code: "+responseCode);

                // Check if the json object was successfully written to heroku.
                if (responseCode == HttpURLConnection.HTTP_OK) {

                    Log.i(CLASS_NAME, "Added successfully");

                } else
                    Log.i(CLASS_NAME, "Failed to add");

                urlConnection.disconnect();

            } catch (Exception e) {
                Log.e(CLASS_NAME, e.getMessage());
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ioe) {
                        //ignore
                    }

                }


                if (urlConnection != null) {
                    try {
                        urlConnection.disconnect();
                    } catch (IllegalStateException ise) {
                        //ignore
                    }
                }
            }
        }

        return null;
    }


}

package com.restobrownegg.signin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.restobrownegg.home.HomeActivity;


/**
 * This class will check if the user has saved data already if it is not the case then, it will
 * promp the user with a sign up form. If its shared preferences has data then it will show
 * the home page.
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class
MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("MainActivity-OnCreate", "App has started");
        Log.i("MainActivity-OnCreate", "Will start retrieve data from shared preferences");
        SharedPreferences prefs = getSharedPreferences("userInfo",MODE_PRIVATE);


        String lastName = prefs.getString("lastName","");
        String firstName = prefs.getString("firstName","");
        String email = prefs.getString("email","");
        String password = prefs.getString("password","");
        String dateAdded = prefs.getString("dateAdded","");
        Log.i("MainActivity-OnCreate", "Finished retrieving data from shared preferences");

        //if empty launch sign up page
        if(lastName.length() == 0 || firstName.length() == 0 ||email.length() == 0
                || password.length()==0||dateAdded.length() ==0)
        {
            Log.i("MainActivity-OnCreate", "Launching SignUpActivity");
            Intent intent = new Intent(this,SignUpActivity.class);
            startActivity(intent);
         }
        //launch the home page
        else
            {
                Log.i("MainActivity-OnCreate", "Launching HomeActivity");
                Intent intent = new Intent(this,HomeActivity.class);
                startActivity(intent);
            }
        finish();
        return;


    }














}

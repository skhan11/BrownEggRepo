package com.restobrownegg.signin;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.restobrownegg.dao.DBHelper;
import com.restobrownegg.home.HomeActivity;
import com.restobrownegg.R;

import java.util.Calendar;

/**
 * This class will handle the Sign up form where user must enter a last name, first name, email and
 * password. It will check if all the fields are valid. If they are then it will store them onto
 * the Shared Preferences and redirect them to the index page. If the fields are invalid then it
 * will show an error message.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class SignUpActivity extends AppCompatActivity {

    private final String CLASS_NAME = "SignUpActivity";
    private boolean isFirstTime = true;

    //getting the editTexts
    private EditText lastName;
    private EditText firstName;
    private EditText email;
    private EditText password;
    private EditText postalCode;
    private Button update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(CLASS_NAME, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        // Checks if this is the first time the user is signing in.
        if (getIntent().getExtras() != null)
            isFirstTime = getIntent().getExtras().getBoolean("firstTime", true);

        lastName = (EditText) findViewById(R.id.editLastName);
        firstName = (EditText) findViewById(R.id.editFirstName);
        email = (EditText) findViewById(R.id.editEmail);
        password = (EditText) findViewById(R.id.editPassword);
        postalCode = (EditText) findViewById(R.id.editPostalCode);

        // If it's not the first time the user is logging in, we assume the user is updating its
        // settings. Therefore, we change the button's text to Update and add a prompt message when
        // the user wants to go back.
        if (!isFirstTime) {
            update = (Button) findViewById(R.id.submitButton);
            update.setText("Update");

            SharedPreferences prefs = getSharedPreferences("userInfo", MODE_PRIVATE);

            lastName.setText(prefs.getString("lastName", ""));
            firstName.setText(prefs.getString("firstName", ""));
            email.setText(prefs.getString("email", ""));
            password.setText(prefs.getString("password", ""));
            postalCode.setText(prefs.getString("postalCode", ""));

            View view = (View) findViewById(android.R.id.content);
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    Log.i(CLASS_NAME, "keyCode: " + keyCode);
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !isFirstTime) {
                        Log.i(CLASS_NAME, "onKey back");
                        cancelButtonClick();
                        return true;

                    } else
                        return false;


                }
            });
        }



    }

    /**
     * Runs when user clicks on the cancel button. It prompts the user for confirmation.
     */
    public void cancelButtonClick() {
        Log.i(CLASS_NAME, "cancelButtonClick");

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);

        builder.setMessage("Are you sure you want to leave?")
                .setTitle("Confirmation");

        // Destroys this activity if the user wants to leave.
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();

            }


        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //dont do anything stay on this page

            }


        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * This event is launched when the user inputs data onto the sign up page.
     * It will validate if the user put valid data then it stores it in shared preferences.
     * This will only happen once.
     *
     * @param view
     */
    public void signInHandler(View view) {
        Log.i(CLASS_NAME, "signInHandler()");
        //error label msg
        TextView errorLabel = (TextView) findViewById(R.id.errorLabel);
        errorLabel.setText("");

        //getting the string from the editTexts
        String strLastName = lastName.getText().toString().trim();
        String strFirstName = firstName.getText().toString().trim();
        String strEmail = email.getText().toString().trim();
        String strPassword = password.getText().toString().trim();
        String strPostalCode = postalCode.getText().toString().trim();
        Log.i(CLASS_NAME, "InfoRetrieved");

        Log.i(CLASS_NAME, "Validating");
        boolean isValid = validateFields(strLastName, strFirstName, strEmail, strPassword, strPostalCode);
        Log.i(CLASS_NAME, "Finished Validating");

        //if all the fields are valid store it in SharedPreferences and open the home activity
        if (isValid) {
            storeData(strLastName, strFirstName, strEmail, strPassword, strPostalCode);
            storeDB(strLastName, strFirstName, strEmail, strPassword, strPostalCode);
            Log.i(CLASS_NAME, "Finishing the app");
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
        //error msg can make a toast if we want
        else {
            Log.i(CLASS_NAME, "Displaying ErrorMsg");
            errorLabel.setText(R.string.errorMessage);
        }


    }


    /**
     * This method checks if the user inputted valid data onto the edit text. It checks the length
     * for each one. However, for the email address it checks whether it is a valid email format.
     * The Patterns.EMAIL_ADDRESS check will return true if it is a valid email address otherwise,
     * it will return false. Patterns in android are commonly used regular expression patterns so I
     * thought it would be a good idea to use it rather than making my own regex.
     *
     * @param lastName
     * @param firstName
     * @param email
     * @param password
     * @param postalCode
     * @return
     */
    private boolean validateFields(String lastName, String firstName, String email, String password, String postalCode) {
        boolean isValid = true;

        if (lastName.length() == 0) {
            Log.i(CLASS_NAME, "Invalid lastname");
            isValid = false;
        }
        if (firstName.length() == 0) {
            Log.i(CLASS_NAME, "Invalid firstName");
            isValid = false;
        }
        if (email.length() == 0 || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Log.i(CLASS_NAME, "Invalid email");
            isValid = false;
        }
        if (password.length() == 0) {
            Log.i(CLASS_NAME, "Invalid password");
            isValid = false;
        }

        if (postalCode.length() == 0) {
            Log.i(CLASS_NAME, "Invalid postalCode");
            isValid = false;
        }

        return isValid;

    }

    /**
     * This method will store user information in SharedPreferences, it will also store a date
     * stamp that will be used checking back end web rest resources. Not sure if this is how
     * we want to store our date might have to make changes.
     * <p>
     * Q: Do we need to hash password before storing it?
     *
     * @param lastName
     * @param firstName
     * @param email
     * @param password
     * @param postalCode
     */
    private void storeData(String lastName, String firstName, String email, String password, String postalCode) {

        Log.i(CLASS_NAME, "Storing in SharedPreferences");
        Calendar c = Calendar.getInstance();
        SharedPreferences prefs = getSharedPreferences("userInfo", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("lastName", lastName);
        editor.putString("firstName", firstName);
        editor.putString("email", email);
        editor.putString("password", password);
        editor.putString("postalCode", postalCode);
        editor.putString("dateAdded", c.getTime() + "");
        editor.commit();

        Log.i(CLASS_NAME, "Finished Storing in SharedPreferences");


    }

    /**
     * Store the user's info in the database if the user doesn't exist in the database already.
     *
     * @param lastName
     * @param firstName
     * @param email
     * @param password
     * @param postalCode
     */
    private void storeDB(String lastName, String firstName, String email, String password, String postalCode) {
        DBHelper helper = new DBHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        int userid = helper.getUserIdByEmail(email, db);

        Log.i(CLASS_NAME, "storeDB userId: " + userid);

        // Check if user exists or not. -1 means it doesn't exist.
        if (userid == -1) {
            helper.addUser(lastName, firstName, email, password, postalCode, db);
        }
    }
}

package com.restobrownegg.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This class manages the SQLite database for the restaurants and users. It has 3 tables : restos,
 * users, and favourites. Restos and Favourites deal with restaurants. Restaurants holds the list
 * of restaurants in the database whereas Favourites holds all the restaurants that a user
 * favourited. This class also has CRUD operations methods that are performed on these 3 tables
 * when its needed.
 *
 * @author Naasir Jusab, Seaim Khan, Shifat Khan, William Ngo, Salman Haider
 */
public class DBHelper extends SQLiteOpenHelper {

    private final String CLASS_NAME = "DBHelper";

    private static DBHelper dbh = null;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "RestoDB";

    // Table names.
    public static final String TABLE_USERS = "users";
    public static final String TABLE_RESTOS = "restos";
    public static final String TABLE_FAVOURITES = "favourites";

    //id used by all tables
    public static final String COL_ID = "_id";

    //dates used by 2 tables RESTOS USERS
    public static final String COL_CREATED_DATE = "created_date";
    public static final String COL_UPDATED_DATE = "updated_date";

    //postal code used by tables RESTOS USERS
    public static final String COL_POSTAL_CODE = "postal_code";


    // Users columns.
    public static final String COL_LAST_NAME = "last_name";
    public static final String COL_FIRSTNAME = "first_name";
    public static final String COL_EMAIL = "email";
    public static final String COL_PASSWORD = "password";


    // Restos columns
    public static final String COL_RESTO_NAME = "resto_name";
    public static final String COL_GENRE = "genre";
    public static final String COL_PRICE_RANGE = "pricing";
    public static final String COL_PHONE_NUMBER = "phone_number";
    public static final String COL_SOURCE = "source";
    public static final String COL_ADDRESS = "address";
    public static final String COL_CITY = "city";
    public static final String COL_LONGITUDE = "longitude";
    public static final String COL_LATITUDE = "latitude";

    //Fav columns used to bridge with restos
    public static final String COL_USER_ID = "user_id";
    public static final String COL_RESTO_ID = "resto_id";

    // Query to create the USERS table.
    private static final String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_LAST_NAME + " TEXT NOT NULL, "
            + COL_FIRSTNAME + " TEXT NOT NULL, "
            + COL_EMAIL + " TEXT NOT NULL, "
            + COL_PASSWORD + " TEXT NOT NULL,"
            + COL_CREATED_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
            + COL_UPDATED_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
            + COL_POSTAL_CODE + " TEXT NOT NULL" +
            ");";

    // Query to create the RESTOS table.
    private static final String CREATE_RESTOS_TABLE = "CREATE TABLE " + TABLE_RESTOS + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_RESTO_NAME + " TEXT NOT NULL,"
            + COL_GENRE + " TEXT NOT NULL,"
            + COL_PRICE_RANGE + " INTEGER NOT NULL,"
            + COL_PHONE_NUMBER + " TEXT NOT NULL,"
            + COL_CREATED_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
            + COL_UPDATED_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
            + COL_ADDRESS + " TEXT NOT NULL, "
            + COL_CITY + " TEXT NOT NULL, "
            + COL_POSTAL_CODE + " TEXT NOT NULL, "
            + COL_LONGITUDE + " REAL, "
            + COL_LATITUDE + " REAL,"
            + COL_SOURCE + " TEXT NOT NULL" +
            ");";

    // Query to create the FAVOURITES table.
    private static final String CREATE_FAVOURITES_TABLE = "CREATE TABLE " + TABLE_FAVOURITES + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_USER_ID + " INTEGER,"
            + COL_RESTO_ID + " INTEGER,"
            + "FOREIGN KEY(" + COL_USER_ID + ") "
            + "REFERENCES " + TABLE_USERS + "(" + COL_ID + "),"
            + "FOREIGN KEY(" + COL_RESTO_ID + ") "
            + "REFERENCES " + TABLE_RESTOS + "(" + COL_ID + ")" +
            ");";


    /**
     * One-parameter constructor.
     * It passes a context, database name and version to be able to perform CRUD operations on
     * a new database.
     *
     * @param context Context
     */
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.i(CLASS_NAME,"Created DBHelper");
    }

    /**
     * Return the DBHelper object.
     * Used to keep state of the database.
     *
     * @param context Context
     * @return
     */
    public static DBHelper getDBHelper(Context context) {
        if (dbh == null)
            dbh = new DBHelper(context.getApplicationContext());

        return dbh;
    }

    /**
     * This method runs when the tables are first created. It will create the users, restos and
     * favourites table. Then, it will add mock data to the restos in order to test different
     * actions in the app.
     * @param db SQLiteDatabase
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(CLASS_NAME, "onCreate()");
        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_RESTOS_TABLE);
        db.execSQL(CREATE_FAVOURITES_TABLE);

        // Populate the database with mock data.
        addMockData(db);
    }


    /**
     * This method runs when the tables already exist, only if the stored version number(oldVersion)
     * is lower than the one passed (newVersion) in the constructor.
     * @param db SQLiteDatabase
     * @param oldVersion int
     * @param newVersion int
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(CLASS_NAME, "onUpgrade()");
        // Drop tables with references to other tables first.
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVOURITES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESTOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);

        //create tables
        onCreate(db);
    }

    /**
     * Populate the database with mock data so we have initial data to work with.
     */
    private void addMockData(SQLiteDatabase db) {
        Log.i(CLASS_NAME, "addMockData()");
        // Arrays containing mock data
        String[] restoNames = {"McDonalds", "BurgerKing", "Thai Express", "Dera Resto", "Papa John's"};
        String[] genre = {"Fast Food", "Fast Food", "Thai", "Indian", "Fast Food"};
        int[] pricing = {1, 2, 3, 3, 2};
        String[] phoneNumber = {"5149267492", "5143746592", "5147360218", "5147234098", "5146296657"};
        String[] address = {"1234 rue Durocher", "4321 Parc", "9999 Roch", "6969 Jarry", "0000 World"};
        String[] city = {"Montreal", "NY", "Lao", "Mumbai", "Avatar"};
        String[] postalCode = {"H3N2Z2", "H4K2N1", "J6H0K3", "L1D8H5", "B2S7I2"};
        double[] latitude = {0, -45, 30, 25, 10};
        double[] longitude = {0, 30, 25, 15, 1};
        String[] source = {"direct input","direct input", "direct input", "direct input", "direct input"};


        // Loop through each array and put it in the database.
        for (int i = 0; i < restoNames.length; i++) {
            addResto(restoNames[i], genre[i], pricing[i], phoneNumber[i], address[i], city[i],
                    postalCode[i],latitude[i],longitude[i], source[i], db);
        }
        Log.i(CLASS_NAME, "finished adding mock data");
    }

    /**
     * Adds a new restaurant in the database by taking in all the fields related to a restaurant.
     * It also takes a writable database in order to insert the restaurant onto the database and
     * to avoid locking the database.
     * @param restoName String
     * @param genre String
     * @param pricing int
     * @param phoneNumber String
     * @param address String
     * @param city String
     * @param postalCode String
     * @param latitude double
     * @param longitude double
     * @param source String
     * @param db SQLiteDatabase
     */
    public void addResto(String restoName, String genre, int pricing, String phoneNumber,
                         String address, String city, String postalCode, double latitude, double longitude,
                         String source, SQLiteDatabase db) {
        Log.i(CLASS_NAME, "addResto()");

        // Insert the restaurant's info in the database.
        ContentValues restoCv = new ContentValues();
        restoCv.put(COL_RESTO_NAME, restoName);
        restoCv.put(COL_GENRE, genre);
        restoCv.put(COL_PRICE_RANGE, pricing);
        restoCv.put(COL_PHONE_NUMBER, phoneNumber);
        restoCv.put(COL_CITY, city);
        restoCv.put(COL_ADDRESS, address);
        restoCv.put(COL_POSTAL_CODE, postalCode);
        restoCv.put(COL_LATITUDE, latitude);
        restoCv.put(COL_LONGITUDE,longitude);
        restoCv.put(COL_SOURCE, source);

        db.insert(TABLE_RESTOS, null, restoCv);
        Log.i(CLASS_NAME,"finished inserting resto: " + restoName);
    }


    /**
     * Adds a particular restaurant to a given user (depicted by its email).
     * Each user will have its own favourites table. Therefore, we pass a user email since its
     * unique and a restoId to keep track of the restos that a user favourited.
     * @param restoId int
     * @param email String
     */
    public void addFavourite(int restoId, String email) {
        Log.i(CLASS_NAME, "addFavourite()");
        SQLiteDatabase db = getWritableDatabase();

        // Get the user's id.
        int userId = getUserIdByEmail(email, db);

        ContentValues favouriteCV = new ContentValues();
        favouriteCV.put(COL_USER_ID, userId);
        favouriteCV.put(COL_RESTO_ID, restoId);

        long id = db.insert(TABLE_FAVOURITES, null, favouriteCV);
        Log.i(CLASS_NAME, "finished addFavourite Fav id: " + id);
    }

    /**
     *  This method takes care of adding a user by taking the fields necessary to insert in the
     *  users table. It also takes a writable database in order to insert the restaurant onto the
     *  database and to avoid locking the database.
     *
     * @param lastName String
     * @param firstName String
     * @param email String
     * @param password String
     * @param db String
     */
    public void addUser(String lastName, String firstName, String email, String password, String postalCode, SQLiteDatabase db) {
        Log.i(CLASS_NAME, "addUser()");

        ContentValues userCV = new ContentValues();
        userCV.put(COL_LAST_NAME, lastName);
        userCV.put(COL_FIRSTNAME, firstName);
        userCV.put(COL_EMAIL, email);
        userCV.put(COL_PASSWORD, password);
        userCV.put(COL_POSTAL_CODE, postalCode);

        long id = db.insert(TABLE_USERS, null, userCV);
        Log.i(CLASS_NAME, "finished addUser userId: " + id);
    }

    /**
     * Return the user's id from a given email. Since an email is unique, we can use that as a unique
     * identifier as well. This method will query to see if a user with a specific email exists.
     * If it is in the database it will return its id otherwise we will get -1.
     *
     * @param email
     * @return the user's id
     */
    public int getUserIdByEmail(String email, SQLiteDatabase db) {
        Log.i(CLASS_NAME, "getUserIdByEmail()");


        Cursor cursor = db.query(TABLE_USERS, new String[]{COL_ID}, COL_EMAIL + "=?", new String[]{email}, null, null, null);

        int userId = -1;
        //since we are expecting one result, it is okay to do move to first
        if (cursor.moveToFirst()){
            userId = cursor.getInt(cursor.getColumnIndex(COL_ID));
        }

        cursor.close();
        Log.i(CLASS_NAME, "getUserIdByEmail userId: " + userId);
        return userId;
    }

    /**
     * Returns a cursor containing information about a singular restaurant identified by its unique id.
     * It will return all the fields from restos in a Cursor object that the receiving end can use
     * to access data of a restaurant with a specific id.
     * @param restoId - restaurant identified by its unique id.
     * @return Cursor containing information about a restaurant
     */
    public Cursor getRestoInfo(int restoId) {
        Log.i(CLASS_NAME, "getRestoInfo()");

        return getReadableDatabase().query(TABLE_RESTOS, null, COL_ID + "= ?",
                new String[]{Integer.toString(restoId)}, null, null, null);
    }

    /**
     * Return a cursor containing information about all the restaurants registered.
     *
     * @return Cursor containing information about all the restaurants.
     */
    public Cursor getAllRestoInfo() {
        Log.i(CLASS_NAME, "getAllRestoInfo()");
        final String query = "SELECT * FROM " + TABLE_RESTOS;

        return getReadableDatabase().rawQuery(query, null);
    }

    /**
     * Return a cursor containing information about all the favourite restaurants for the current
     * logged in user (identified by its email).
     *
     * @return Cursor containing information about all the favourite restaurants.
     */
    public Cursor getFavouriteRestos(String email) {
        Log.i(CLASS_NAME, "getFavouriteRestos()");

        final String query = "SELECT " + COL_RESTO_NAME + ", " + COL_GENRE + ", " + COL_ADDRESS + ", " + COL_PRICE_RANGE + ", " + COL_LATITUDE + ", " + COL_LONGITUDE + ", "
                + TABLE_RESTOS+ "." + COL_ID + ", " + COL_PHONE_NUMBER +" FROM " + TABLE_RESTOS
                + " INNER JOIN " + TABLE_FAVOURITES
                + " ON " + TABLE_RESTOS + "." + COL_ID + " = " + TABLE_FAVOURITES + "." + COL_RESTO_ID
                + " INNER JOIN " + TABLE_USERS
                + " ON " + TABLE_FAVOURITES + "." + COL_USER_ID + " = " + TABLE_USERS + "." + COL_ID
                + " WHERE " + COL_EMAIL + " = ?";

        return getReadableDatabase().rawQuery(query, new String[]{email});
    }

    /**
     * Checks if the restaurant exists or not in the database. Returns a boolean indicating if
     * the resto exists already. Takes in multiple params to check the database.
     * It will check if a restaurant exists by checking whether fields like restoName, restoGenre,
     * restoPrice, latitude, longitude are already in the database. If it is already in the database
     * then the count will be greater than 1 if it is not then in the database then it will be less
     * than 1 which means that the resto does no exist.
     *
     * @param restoName String
     * @param restoGenre String
     * @param restoPrice int
     * @param latitude double
     * @param longitude double
     * @return Boolean indicating if the resto exists already
     */
    public boolean checkRestoExists(String restoName, String restoGenre, int restoPrice, double latitude, double longitude)
    {
        Log.i(CLASS_NAME, "checkRestoExists()");
        final String query =  " SELECT * FROM " + TABLE_RESTOS
                            + " WHERE " + COL_RESTO_NAME + " = ? AND "
                            + COL_GENRE + " = ? AND "
                            + COL_PRICE_RANGE + " = ? AND "
                            + COL_LATITUDE + " = ? AND "
                            + COL_LONGITUDE + " = ?;";

        Cursor cursor = getReadableDatabase().rawQuery(query, new String[]{restoName, restoGenre, Integer.toString(restoPrice),
                                                                            Double.toString(latitude),Double.toString(longitude)});
        Log.i(CLASS_NAME,cursor.getCount()+"");
       return cursor.getCount() >= 1;
    }


    /**
     * This method will pass fields from a restaurant to get its id. It will check these parameters
     * exist in the database. If it does exist then it will return the appropriate id of the restaurant
     * otherwise, it will return -1 which means that the resto does not exist.
     * @param restoName String
     * @param restoGenre String
     * @param restoPrice int
     * @param latitude double
     * @param longitude double
     * @return int restaurant's id
     */
    public int getRestoId(String restoName, String restoGenre, int restoPrice, double latitude, double longitude)
    {
        Log.i(CLASS_NAME, "getRestoId()");
        final String query =  " SELECT " + TABLE_RESTOS+ "." + COL_ID + " FROM " + TABLE_RESTOS
                + " WHERE " + COL_RESTO_NAME + " = ? AND "
                + COL_GENRE + " = ? AND "
                + COL_PRICE_RANGE + " = ? AND "
                + COL_LATITUDE + " = ? AND "
                + COL_LONGITUDE + " = ?;";
        Cursor cursor = getReadableDatabase().rawQuery(query, new String[]{restoName,
                restoGenre,
                restoPrice+"",
                Double.toString(latitude),
                Double.toString(longitude)});

        int restoId =-1;
        if (cursor.moveToFirst()){
            restoId = cursor.getInt(cursor.getColumnIndex(COL_ID));
        }

        cursor.close();
        Log.i(CLASS_NAME, "getRestoId: " + restoId);

        return restoId;

    }

    /**
     * Searches through the resto table and returns any restaurants with similar names compared to
     * the keyword and similar to the genre. This search function takes these two parameters into
     * account and then it will search the database to return the restos that match these two params.
     *
     * @param keyword string to search a restaurant
     * @return Cursor containing restos searched.
     */
    public Cursor searchRestos(String keyword, String genre){
        Log.i(CLASS_NAME, "searchRestos()");
        keyword = '%'+keyword+'%';
        genre = '%'+genre+'%';

        return getReadableDatabase().rawQuery("SELECT * FROM " + TABLE_RESTOS +
                " WHERE " + COL_RESTO_NAME + " LIKE ? AND " + COL_GENRE + " LIKE ?;",new String[]{keyword, genre});
    }
}
